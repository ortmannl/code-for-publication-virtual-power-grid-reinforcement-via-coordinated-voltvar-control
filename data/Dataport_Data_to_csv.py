import pandas as pd

# import complete Dataport data (Austin, 25 households, 1 year), (New York, 25 households, 6 months),
# (California, 25 households, 1 year) from .csv file

# time resolution
res = '1min'
# location from ['austin', 'newyork', 'california']
location = 'austin'

data_complete = pd.read_csv('data/dataport/' + res + 'ute_data_' + location + '.csv', header=0, index_col=['dataid'],
                   usecols=['dataid', 'localminute', 'grid', 'solar', 'solar2'])

# get all DataIDs of the available households
dataIDs = list(set(data_complete.index))
dataIDs.sort()

# Put data into nicer format

# start from empty dataframe
dataport_data = pd.DataFrame()
for ID in dataIDs:  # for each household
    data_of_ID = data_complete.loc[ID]  # take only the current household from complete data
    data_of_ID.set_index('localminute', inplace=True)   # use timestamp as new index
    # data_of_ID.drop('dataid', inplace=True, axis=1)
    data_of_ID.columns = ['grid_' + str(ID), 'solar_' + str(ID), 'solar2_' + str(ID)]   # add householdID to each row for distinguishing
    dataport_data = pd.concat([dataport_data, data_of_ID], axis=1, join="outer")    # add household to new format dataframe

# sort data by timestamp to bring everything into order
dataport_data = dataport_data.sort_index()

# manually correct corrupted datapoints
dataport_data.at['2018-02-02 12:26:00-06', 'grid_7536'] = 0.0
dataport_data.at['2018-02-02 12:26:00-06', 'solar_7536'] = 0.0
dataport_data.at['2018-02-02 12:27:00-06', 'grid_7536'] = 0.0
dataport_data.at['2018-02-02 12:27:00-06', 'solar_7536'] = 0.0

dataport_data.at['2018-03-30 11:42:00-05', 'grid_9160'] = 0.0
dataport_data.at['2018-03-30 11:42:00-05', 'solar_9160'] = 0.0


dataport_data.to_csv('data/dataport/dataport_data_' + location + '_1min.csv')