import numpy as np
import pandas as pd
import datetime
import matplotlib.pyplot as plt
import random
from scipy.signal import savgol_filter
import os

data_dir = os.path.join(os.path.dirname(__file__), 'dataport/')

def load_data_1day(net, location, res, day, month):

    if location not in ['austin', 'newyork', 'california']:
        raise ValueError("Not a valid location for the available Dataport data")

    if res not in ['1min']:
        raise ValueError("Not a valid temporal resolution for the available Dataport data")

    if location in ['austin', 'california']:
        year = 2018
        start_month, start_day = 1, 1
    elif location == 'newyork':
        year = 2019
        start_month, start_day = 5, 1

    # sample date
    date = datetime.datetime(year=year, month=month, day=day, tzinfo=datetime.timezone(datetime.timedelta(hours=-6)))

    # determine which rows to import and only import them to save time
    start_of_data = datetime.datetime(year=year, month=start_month, day=start_day,
                                      tzinfo=datetime.timezone(datetime.timedelta(hours=-6)))

    if res == '1min':
        n_steps = 24 * 60
    else:
        raise ValueError("Resolution not valid. N_steps could not be determined.")

    start_row = (date - start_of_data).days * n_steps

    # load dataport data here
    data = pd.read_csv(f'{data_dir}/dataport_data_{location}_{res}.csv', header=0, index_col=0,
                       parse_dates=True, skiprows=list(range(1, start_row+1)), nrows=n_steps,
                       date_parser=lambda col: pd.to_datetime(col, utc=True))

    # drop generation columns which correspond to houses without PV (with at least 80% nan)
    data.dropna(axis='columns', thresh=int(0.8 * n_steps), inplace=True)
    data.interpolate(method='linear', axis='columns', inplace=True)

    dataIDs_load = sorted(list(set([name[5:] for name in data.columns if name.startswith('grid_')])),
                          key=lambda x: int(x))
    dataIDs_generation = sorted(list(set([name[6:] for name in data.columns if name.startswith('solar_')])),
                                key=lambda x: int(x))

    # intialize variables
    number_loads = len(net.load.index)
    available_loads = len(dataIDs_load)
    available_gens = len(dataIDs_generation)

    load_data = np.zeros((n_steps, available_loads))
    generation_data = np.zeros((n_steps, available_gens))

    for i, ID in enumerate(dataIDs_load):
        # total load: grid (consumed - produced) + generation (produced) = consumed
        load_data[:, i] = data['grid_' + str(ID)].to_numpy(na_value=0.0)
        if 'solar_' + str(ID) in data.columns:
            load_data[:, i] += data['solar_' + str(ID)].to_numpy(na_value=0.0)
        if 'solar2_' + str(ID) in data.columns:
            load_data[:, i] += data['solar2_' + str(ID)].to_numpy(na_value=0.0)

    for i, ID in enumerate(dataIDs_generation):
        # generation: add solar and solar2 (if exists) of current household
        generation_data[:, i] = data['solar_' + str(ID)].to_numpy(na_value=0.0)
        if 'solar2_' + str(ID) in data.columns:
            generation_data[:, i] += data['solar2_' + str(ID)].to_numpy(na_value=0.0)

    return load_data, generation_data, dataIDs_load, dataIDs_generation, date


def prepare_1day_deterministic(net, location, res, day, month, n_loads_per_bus, load_scaling, pv_scaling):

    load_data, generation_data, data_id_load, data_id_generation, date = load_data_1day(net, location, res, day, month)

    n_steps = load_data.shape[0]
    available_loads = load_data.shape[1]
    available_gens = generation_data.shape[1]
    number_loads = len(net.load.index)

    # initalize load and generation matrices
    load_t = np.zeros((number_loads, n_steps))
    generation_t = np.zeros((number_loads, n_steps))

    i_load = 0
    i_gen = 0

    data_distribution_to_buses = {}

    # for all load buses
    for bus in range(number_loads):
        current_bus = net.load.bus.iloc[bus]
        current_bus_name = net.bus.name[current_bus][4:]
        # create dictionary which contains information about which data is used at each bus
        data_distribution_to_buses['Load ' + current_bus_name] = []
        data_distribution_to_buses['SGen ' + current_bus_name] = []
        # number of houses at current bus
        loads_at_current_bus = n_loads_per_bus[current_bus_name]

        # add the aggregated loads of the correct houses to current bus
        for _ in range(loads_at_current_bus):
            load_t[bus, :] += load_data[:, i_load]
            data_distribution_to_buses['Load ' + current_bus_name].append(data_id_load[i_load])
            i_load += 1
            if i_load == available_loads:
                i_load = 0

        for load_id in data_distribution_to_buses['Load ' + current_bus_name]:
            if load_id in data_id_generation:
                i_gen = data_id_generation.index(load_id)
                generation_t[bus, :] += generation_data[:, i_gen]
                data_distribution_to_buses['SGen ' + current_bus_name].append(data_id_generation[i_gen])

    # load scaling
    load_t = load_scaling * load_t  # scale load
    generation_t = pv_scaling * generation_t

    # print(data_distribution_to_buses)

    t = np.arange(0, n_steps, step=1)

    # convert to MW / Dataport data is in kW
    return t, np.transpose(load_t) * 1e-3, np.transpose(generation_t) * 1e-3, date


def present_data(month=1, day=1, location='austin'):

    # presents one day of the data
    # Data import

    if location not in ['austin', 'newyork', 'california']:
        raise ValueError("Not a valid location for the available Dataport data")

    if location in ['austin', 'california']:
        year = 2018

    elif location == 'newyork':
        year = 2019


    res = '1min'

    data = pd.read_csv('data/dataport/dataport_data_' + location + '_' + res + '.csv', header=0, index_col=0,
                       parse_dates=True, date_parser=lambda col: pd.to_datetime(col, utc=True))

    dataIDs = [name[5:] for name in data.columns[::3]]

    print("Import done")

    # Put data of sample day into numpy arrays and plot data

    # define sample date
    date = datetime.datetime(year=year, month=month, day=day, tzinfo=datetime.timezone(datetime.timedelta(hours=-6)))

    start = date
    end = date + datetime.timedelta(days=1)

    # select data corresponding to sample date
    data_day = data.loc[start:end, :]
    n_steps = len(data_day.index)
    n_houses = len(data_day.columns)

    # store in numpy arrays
    # load and generation of all residential households for selected day

    load_data = np.zeros((n_steps, n_houses))
    generation_data = np.zeros((n_steps, n_houses))

    for i, ID in enumerate(dataIDs):
        # generation: add solar and solar2 of current household
        generation_data[:, i] = data_day['solar_' + str(ID)].to_numpy(na_value=0.0) + data_day[
            'solar2_' + str(ID)].to_numpy(na_value=0.0)
        # total load: grid (consumed - produced) + generation (produced) = consumed
        load_data[:, i] = data_day['grid_' + str(ID)].to_numpy(na_value=0.0) + generation_data[:, i]

    t = np.arange(0, n_steps, step=1)

    # Drop corrupted data
    only_good_data = False
    if only_good_data:
        good_dataIDs = [661, 1642, 2335, 2818, 3456, 3538, 4031, 4373, 4767, 5746,
                        6139, 7536, 7719, 7800, 7901, 8156, 8386, 8565, 9019, 9160]
    else:
        good_dataIDs = dataIDs
    good_houses_index = [i for i in range(len(dataIDs)) if dataIDs[i] in good_dataIDs]

    load_data = load_data[:, good_houses_index]
    generation_data = generation_data[:, good_houses_index]

    n_houses = len(good_dataIDs)

    # Plot Data
    plot_data = True
    if plot_data:

        # specific household
        # houses_of_interest = [10, 11]
        # houseID = '2361'
        # houses_of_interest = dataIDs.index(houseID)
        # all houses
        houses_of_interest = list(range(0, n_houses))

        # plot generation and consumption
        # create subplots
        fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 11))
        fig.suptitle('Dataport Data: ' + str(date)[0:10], fontsize=20)

        # ax1: plot loads
        ax1.set_title('Total Consumption of Households (kW)')
        ax1.plot(t, load_data[:, houses_of_interest], linewidth=0.5)
        ax1.set_xlim(0, n_steps)
        ax1.set_xticks([n_steps // 24 * s for s in range(0, 25)])
        ax1.set_xticklabels(list(range(0, 25)), fontsize=8)
        ax1.set_ylabel('Load [kW]')
        ax1.set_xlabel('Time [h]')
        ax1.legend(good_dataIDs, loc='right')

        # ax2: plot active power generation at all buses
        ax2.set_title('Total Solar Generation of Households (kW)')
        ax2.plot(t, generation_data[:, houses_of_interest], linewidth=0.5)
        ax2.set_xlim(0, n_steps)
        ax2.set_xticks([n_steps // 24 * s for s in range(0, 25)])
        ax2.set_xticklabels(list(range(0, 25)), fontsize=8)
        ax2.set_ylabel('Generation [kW]')
        ax2.set_xlabel('Time [h]')
        ax2.legend(good_dataIDs, loc='right')

        # highlight a specific bus in the voltage plot:
        highlight = False
        highlight_colors = ['orange', 'green', 'red', 'blue', 'brown']
        current_color = 0
        if highlight:
            idx_bus_to_highlight = [0]
            for i, line in enumerate(ax1.lines):
                if i in idx_bus_to_highlight:
                    line.set_color(highlight_colors[current_color])
                    current_color += 1
                else:
                    line.set_color('gray')
            ax1.legend(dataIDs, loc='right')

        plt.show()


def smoothen_data(load_data, gen_data, method='rolling'):

    if method == 'rolling':
        kernel_size = 21
        kernel = np.ones(kernel_size) / kernel_size
        print(load_data.shape)
        for i in range(load_data.shape[1]):
            load_data[:, i] = np.convolve(load_data[:, i], kernel, mode='same')
            gen_data[:, i] = np.convolve(gen_data[:, i], kernel, mode='same')

    if method == 'savgol':
        load_data = savgol_filter(load_data, 21, 2, axis=0)
        gen_data = savgol_filter(gen_data, 21, 2, axis=0)

    return load_data, gen_data
