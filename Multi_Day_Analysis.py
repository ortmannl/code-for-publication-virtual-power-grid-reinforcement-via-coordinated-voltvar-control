import pandas as pd
import numpy as np
import datetime
from os.path import exists

import Simulation_Settings as params
import Metrics
import Plot_Results as pltres


start_date = datetime.date(month=1, day=1, year=2018)
end_date = datetime.date(month=12, day=31, year=2018)
date_range = pd.date_range(start_date, end_date)
n_days = len(date_range)

controls = ['droop', 'ml-droop', 'fo', 'opf']
pv_scenarios = ['2020', '2030', '2035']

timestep = int(params.res[:-3])

save_plots = True

daily_metrics = {
    'total injected Q (MVArh)': np.zeros(n_days),
    'total consumed Q (MVArh)': np.zeros(n_days),
    'total injected Q squared (MVAr2h)': np.zeros(n_days),
    'total consumed Q squared (MVAr2h)': np.zeros(n_days),
    'maximum injected Q (MVAr)': np.zeros(n_days),
    'maximum consumed Q (MVAr)': np.zeros(n_days),
    'maximum bus voltage (p.u.)': np.ones(n_days),
    'minimum bus voltage (p.u.)': np.ones(n_days),
    'duration of overvoltage (h)': np.zeros(n_days),
    'duration of undervoltage (h)': np.zeros(n_days),
    'integral of overvoltage (Vh)': np.zeros(n_days),
    'integral of undervoltage (Vh)': np.zeros(n_days),
    'total unnecessary losses in grid (MWh)': np.zeros(n_days)
}

total_metrics = {
    'total injected Q (MVArh)': 0,
    'total consumed Q (MVArh)': 0,
    'total injected Q squared (MVAr2h)': 0,
    'total consumed Q squared (MVAr2h)': 0,
    'duration of overvoltage (h)': 0,
    'duration of undervoltage (h)': 0,
    'integral of overvoltage (Vh)': 0,
    'integral of undervoltage (Vh)': 0,
    'total unnecessary losses in grid (MWh)': 0
}

voltage_histo = {
    'x': None,
    'y': None
}

missing_files = 0
tasks = {
    "compute": False,
    "plot": True
}

if tasks["compute"]:
    print("Computing multi-day metrics...")

    for pv_scenario in pv_scenarios:
        for control in controls:

            all_voltages = []

            for i, date in enumerate(date_range):

                if not exists(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv'):
                    missing_files += 1
                    continue

                v = np.genfromtxt(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv', delimiter=',')
                q = np.genfromtxt(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_q_{date.month}-{date.day}.csv', delimiter=',')
                p_losses = np.genfromtxt(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_p_losses_{date.month}-{date.day}.csv', delimiter=',')
                v_no_control = np.genfromtxt(f'simulation_results/none/{pv_scenario}/{pv_scenario}_none_v_{date.month}-{date.day}.csv', delimiter=',')
                
                metrics = Metrics.calculate_time_series_metrics(v, q, p_losses, v_no_control)

                all_voltages.append(np.ravel(v[params.load_bus_idx, :]))

                for metric in daily_metrics:
                    daily_metrics[metric][i] = metrics[metric]

                voltage_histo_day = Metrics.voltage_histogram(v)
                if i == 0:
                    voltage_histo = voltage_histo_day
                voltage_histo['y'] += voltage_histo_day['y']

            for metric in daily_metrics:
                np.savetxt(f'metrics_results/daily/{pv_scenario}_{control}_{metric}.csv', daily_metrics[metric], delimiter=',')

            for axis in voltage_histo:
                np.savetxt(f'metrics_results/total/{pv_scenario}_{control}_voltage-histo_{axis}.csv',
                           voltage_histo[axis], delimiter=',')

            np.savetxt(f'metrics_results/total/{pv_scenario}_{control}_all-voltages.csv',
                       np.concatenate(all_voltages), fmt='%.6e', delimiter=',')

if tasks["plot"]:
    print("Plotting multi-day metrics...")

    total_controller_metrics = {}

    for pv_scenario in pv_scenarios:

        daily_controller_metrics = {}
        for control in controls:
            daily_controller_metrics[control] = {}
            for metric in daily_metrics:
                daily_controller_metrics[control][metric] = np.genfromtxt(f'metrics_results/daily/{pv_scenario}_{control}_{metric}.csv', delimiter=',')

        total_controller_metrics[pv_scenario] = {}
        for control in controls:
            total_controller_metrics[pv_scenario][control] = {}
            for metric in total_metrics:
                total_controller_metrics[pv_scenario][control][metric] = np.sum(daily_controller_metrics[control][metric])

        voltage_histo = {}
        all_voltages = {}
        for control in controls:
            voltage_histo[control] = {
                'x': np.genfromtxt(f'metrics_results/total/{pv_scenario}_{control}_voltage-histo_x.csv', delimiter=','),
                'y': np.genfromtxt(f'metrics_results/total/{pv_scenario}_{control}_voltage-histo_y.csv', delimiter=',')
            }
            all_voltages[control] = np.genfromtxt(f'metrics_results/total/{pv_scenario}_{control}_all-voltages.csv', delimiter=',')
            # total voltage histogram
            # pltres.plot_histogram_voltages(voltage_histo[control], control, pv_scenario, save=False)

        pltres.plot_metrics_bar(total_controller_metrics[pv_scenario], all_voltages, controls, pv_scenario, save=save_plots)

    print("=== Unnecessary line losses (MWh) ===")
    for pv_scenario in pv_scenarios:
        for control in controls:
            print(f"{pv_scenario} / {control}: {total_controller_metrics[pv_scenario][control]['total unnecessary losses in grid (MWh)']}")

    pltres.comparison_scenarios(total_controller_metrics, controls, pv_scenarios, save=save_plots)
