from simulate import One_Day_Simulation as oneday
import Plot_Results as pltres
import grid.Prepare_CIGRE_Residential as cigre
import Simulation_Settings as params
import Metrics
import datetime
from os.path import exists
import numpy as np


def sunny_summer_day():

    # scenario for PV integration
    pv_scenarios = ['2035']     # ['2020', '2030', '2035']

    # control method
    controls = ['none', 'droop', 'fo'] # , 'opf']   # ['none', 'droop', 'ml-droop', 'fo', 'opf']

    use_prior_simulation_results = {'none': False, 'droop': False, 'ml-droop': False, 'fo': False, 'opf': True}
    plot = {
        'data': False,
        'v_var': False,
        'v_histo': False,
        'line_loading': False,
        'metrics': False,
        'comparison_scenarios': False,
        'q_by_bus': False
    }
    save_plots = True

    # data from July 2
    date = datetime.date(month=7, day=2, year=1000)  # year is chosen later according to location to match available data

    # prepare CIGRE LV grid (European, only residential part)
    net = cigre.prepare_european_cigre_lv_residential(load_buses=params.load_bus_configuration)

    metrics = {pv_scenario: {} for pv_scenario in pv_scenarios}
    voltage_histos = {pv_scenario: {} for pv_scenario in pv_scenarios}
    all_voltages = {pv_scenario: {} for pv_scenario in pv_scenarios}

    for pv_scenario in pv_scenarios:
        for j, control in enumerate(controls):

            if use_prior_simulation_results[control] and exists(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv'):
                vm_buses = np.genfromtxt(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv', delimiter=',')
                q_gens = np.genfromtxt(f'simulation_results/{control}/{pv_scenario}/{pv_scenario}_{control}_q_{date.month}-{date.day}.csv', delimiter=',')
                t = np.array(list(range(vm_buses.shape[1])))
                print(f"Re-used simulation results for {control} control.")
            else:
                print(f"Run the sunny-summer-day simulation using {control} control...")
                t, load_data, gen_data, vm_buses, q_gens, i_loading, p_losses = oneday.one_day_simulation(net, control, pv_scenario, date)

                if plot['data']:
                    pltres.plot_data(net, t, load_data, gen_data, pv_scenario, mode='standard', save=save_plots)

            # plot results
            # voltages and var control
            if plot['v_var']:
                pltres.plot_v_q(net, t, vm_buses, q_gens, pv_scenario, control, highlight=list(net.sgen.bus), mode='standard', save=save_plots)
            # line loading
            if plot['line_loading']:
                pltres.plot_line_loading(net, t, i_loading, save=save_plots)

            # voltage histogram
            voltage_histo = Metrics.voltage_histogram(vm_buses)
            voltage_histos[pv_scenario][control] = voltage_histo
            if plot['v_histo']:
                pltres.plot_histogram_voltages(voltage_histo, control, pv_scenario, save=save_plots)

            # compute metrics
            metrics[pv_scenario][control] = Metrics.calculate_time_series_metrics(vm_buses, q_gens, p_losses)
            all_voltages[pv_scenario][control] = np.ravel(vm_buses[2:, :])

        if plot['metrics']:
            pltres.plot_metrics_bar(metrics[pv_scenario], all_voltages[pv_scenario], controls, pv_scenario, save=save_plots)

    if plot['comparison_scenarios']:
        pltres.comparison_scenarios(metrics, controls, pv_scenarios, save=save_plots)

    print(metrics)


if __name__ == "__main__":
    sunny_summer_day()
