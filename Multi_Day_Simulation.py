from simulate import One_Day_Simulation as oneday
from grid import Prepare_CIGRE_Residential as cigre
import Simulation_Settings as params

import numpy as np
import datetime
import time
import multiprocess as mp
import pandas as pd
from os.path import exists
from tqdm import tqdm
import os

results_dir = os.path.join(os.path.dirname(__file__), 'simulation_results')


def run_day(arguments):
    net, control, date, pv_scenario = arguments

    # print(f"Start evaluation for {pv_scenario}, {control}, {date.date}")
    try:
        t, load_data, gen_data, vm_buses, q_gens, i_loading, p_losses = \
            oneday.one_day_simulation(net, control, pv_scenario, date)
        if not os.path.exists(f'{results_dir}/{control}/{pv_scenario}'):
            os.makedirs(f'{results_dir}/{control}/{pv_scenario}')
        np.savetxt(f'{results_dir}/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv', vm_buses, fmt='%.9e', delimiter=',')
        np.savetxt(f'{results_dir}/{control}/{pv_scenario}/{pv_scenario}_{control}_q_{date.month}-{date.day}.csv', q_gens, fmt='%.9e', delimiter=',')
        np.savetxt(f'{results_dir}/{control}/{pv_scenario}/{pv_scenario}_{control}_p_losses_{date.month}-{date.day}.csv', p_losses, fmt='%.9e', delimiter=',')
    except Exception as e:
        print(f"{date}: {e}")


def run_multi_day_parallel(arguments):
    if __name__ == '__main__':
        # PARALLEL PROCESSING
        print("CPU-count: " + str(mp.cpu_count()))
        pool = mp.Pool(mp.cpu_count())
        start = time.time()
        print("Start multi-day simulation using parallel processing")
        with tqdm(total=len(arguments)) as pbar:
            for _ in pool.imap_unordered(run_day, arguments):
                pbar.update()
        pool.close()
        pool.join()
        end = time.time()
        print(f"Simulation of the multi-day time series took {round((end - start) / 60, 2)} min.")


if __name__ == "__main__":

    # scenario for PV integration
    pv_scenarios = ['2020', '2030', '2035']     # ['2020', '2030', '2035']
    # control methods
    controls = ['none', 'droop', 'ml-droop', 'fo', 'opf']

    start_date = datetime.date(month=1, day=1, year=2018)
    end_date = datetime.date(month=12, day=31, year=2018)

    # prepare CIGRE LV grid (European, only residential part)
    net = cigre.prepare_european_cigre_lv_residential(load_buses=params.load_bus_configuration)

    arguments = []
    for date in pd.date_range(start_date, end_date):
        for pv_scenario in pv_scenarios:
            for control in controls:
                if exists(f'{results_dir}/{control}/{pv_scenario}/{pv_scenario}_{control}_v_{date.month}-{date.day}.csv') and exists(f'{results_dir}/{control}/{pv_scenario}/{pv_scenario}_{control}_q_{date.month}-{date.day}.csv'):
                    continue
                else:
                    arguments.append((net, control, date, pv_scenario))

    run_multi_day_parallel(arguments)
