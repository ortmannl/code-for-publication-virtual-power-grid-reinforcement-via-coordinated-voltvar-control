import numpy as np
from scipy.optimize import minimize
from scipy.optimize import LinearConstraint
import Simulation_Settings as params
from pandapower import runpp


def linear_pgd_ortmann(M, A, b, C, d, H, u, y, lambda_t, alpha):
    """Does a step of the linear projected gradient descent for a generic problem formulation."""

    lambda_t1 = np.maximum(lambda_t + alpha * (A @ y - b), np.zeros(len(lambda_t)))

    # anti-windup (from Ortmann)
    if params.anti_windup:
        for i in range(len(lambda_t) // 2):
            j = i + len(lambda_t) // 2
            # if y[i] < -b[i] and (u == d[len(d)//2:]).all():
            if y[i] < -b[i] and np.allclose(u, d[len(d)//2:], atol=1e-8, rtol=0):
                # print("lower anti-windup")
                lambda_t1[i] = lambda_t[i] # lambda_t[i] / 2 # np.zeros_like(lambda_t[i])
            # if y[i] > b[j] and (u == -d[:len(d)//2]).all():
            if y[i] > b[j] and np.allclose(u, -d[:len(d)//2], atol=1e-8, rtol=0):
                # print("upper anti-windup")
                lambda_t1[j] = lambda_t[j] #  / 2 # np.zeros_like(lambda_t[j]) # lambda_t[j]

    u_unc = -np.linalg.inv(M) @ H.T @ A.T @ lambda_t1

    constraint_u = LinearConstraint(C, np.repeat(-np.inf, len(d)), d)
    min_fun = lambda w: np.transpose(w - u_unc) @ M @ (w - u_unc)
    u_new = minimize(min_fun, x0=u, constraints=constraint_u, tol=1e-12).x

    return u_new, lambda_t1


def apply_fo(net, lambda_t, var_limits):

    v_min, v_max = params.v_min + params.tighten_v_constraints, params.v_max - params.tighten_v_constraints
    alpha = params.alpha
    H = params.H_sensitivity_matrix

    n_loads = len(net.sgen.index)
    assert(n_loads == len(var_limits))

    M = np.eye(n_loads)
    A = np.concatenate((-np.eye(n_loads), np.eye(n_loads)))
    b = np.concatenate((np.repeat(-v_min, n_loads), np.repeat(+v_max, n_loads)))
    C = np.concatenate((-np.eye(n_loads), np.eye(n_loads)))

    # reactive power capabilities
    q_min = -var_limits
    q_max = var_limits
    # input constraints
    d = np.concatenate((-q_min, q_max))
    # print(d)

    # no model information
    # H = np.ones((n_loads, n_loads))     # the simplest sensitivity matrix possible

    count_ofo_steps = 0
    for _ in range(params.max_fo_steps):
        # current q_var set-points
        u = net.sgen['q_mvar'].to_numpy()
        vm_pu = net.res_bus['vm_pu'].to_numpy()
        # y = voltages buses where gens are located
        y = np.zeros(len(net.load.index))
        for i, bus in enumerate(net.sgen.bus):
            y[i] = vm_pu[bus]
        # do step of linear Projected Gradient Descent as in Ortmann
        count_ofo_steps += 1
        q_t1, lambda_t = linear_pgd_ortmann(M, A, b, C, d, H, u, y, lambda_t, alpha)
        net.sgen['q_mvar'] = q_t1
        # let power flow converge before doing the next OFO step
        runpp(net)
        if np.allclose(vm_pu, net.res_bus['vm_pu'].to_numpy(), atol=1e-12, rtol=0):
            break
    # print("OFO steps:", count_ofo_steps)

    lambda_t1 = lambda_t
    return lambda_t1
