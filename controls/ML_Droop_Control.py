import numpy as np
import Simulation_Settings as params


def droop_func(v, var_limit, s, beta):

    max_slope = params.maximum_droop_slope / var_limit
    q_min = -var_limit
    q_max = var_limit

    if v < 0.95:
        return q_max
    elif v > 1.05:
        return q_min

    else:
        beta0 = 0
        q = 1 + beta0 * v
        q += sum([beta[k] * (v - s[k]) * (v > s[k]) for k in range(len(s))])
        q = max(q, -max_slope * (v - 0.95) + 1)
        q = min(q, -max_slope * (v - 1.05) - 1)
        q *= q_max

        if q < q_min:
            return q_min
        elif q > q_max:
            return q_max
        else:
            return q


def apply_droop(net, q_t, var_limits):

    droop_curves = "controls/ml_droop/droop_curves/"
    q_t1 = np.zeros(len(net.sgen.index))
    low_pass_lambda = params.low_pass_lambda

    for i in net.sgen.index:
        # bus to which sgen i is connected
        bus = net.sgen.bus[i]
        # v_m at bus to which sgen i is connected
        v_m = net.res_bus.at[bus, 'vm_pu']
        # calculate injected/consumed q_mvar according to droop control function
        s = np.genfromtxt(droop_curves + 'gen_' + str(i) + '_s.csv', delimiter=',')
        beta = np.genfromtxt(droop_curves + 'gen_' + str(i) + '_beta_mw.csv', delimiter=',')
        if low_pass_lambda is not None:
            q_t1[i] = (1 - low_pass_lambda) * q_t[i] + low_pass_lambda * droop_func(v_m, var_limits[i], s, beta)
        else:
            q_t1[i] = droop_func(v_m, var_limits[i], s, beta)
        # apply calculated q_mvar to net
        net.sgen.loc[i, 'q_mvar'] = q_t1[i]

    return q_t1
