import numpy as np
import matplotlib.pyplot as plt
from controls import ML_Droop_Control as mldroop

plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.sans-serif': 'Times New Roman'})
plt.rcParams.update({'mathtext.fontset': 'dejavuserif'})

FONTSCALE = 1.5

plt.rc('font', size=12*FONTSCALE)          # controls default text sizes
plt.rc('axes', titlesize=15*FONTSCALE)     # fontsize of the axes title
plt.rc('axes', labelsize=13*FONTSCALE)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=12*FONTSCALE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=12*FONTSCALE)    # fontsize of the tick labels
plt.rc('legend', fontsize=11*FONTSCALE)    # legend fontsize
plt.rc('figure', titlesize=15*FONTSCALE)   # fontsize of the figure title
suptitlesize = 20*FONTSCALE

LOAD_BUSES = ["R11", "R15", "R16", "R17", "R18"]


def plot_droop_curve(ax, gen, v_lim, q_max, s, beta, color):

    v_min, v_max = v_lim
    v = np.linspace(v_min, v_max, 1000)
    q = np.array([mldroop.droop_func(v, q_max, s, beta) for v in v])
    assert(len(q) == len(v))

    # ax.plot(v, q * 1000, color=color, label="SGen " + str(gen))
    ax.plot(v, q/q_max, color=color, label=LOAD_BUSES[gen], lw=3.0, zorder=-1)


def plot_q_of_v_scatter(ax, gen, v, q, v_lim, q_max, color):

    ax.plot(np.transpose(v[gen, :]), np.transpose(q[gen, :])/q_max,
    # ax.plot(np.transpose(v), np.transpose(q) / q_max,
            color=color, ls='', marker='.', markersize=3, alpha=0.3)
    ax.set_xlabel("Bus voltage [p.u.]")
    ax.set_ylabel("Reactive Power [q/q_max]")
    ax.set_xlim(v_lim)


def plot_scatter_and_droop_curves(gens, v, q, q_max_gens, dirs, algo_params):

    figsize = (12, 7)

    fig, ax = plt.subplots(1, 1, figsize=figsize)
    colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple"]

    for gen in gens:

        s = np.genfromtxt(dirs[1] + 'gen_' + str(gen) + '_s.csv', delimiter=',')
        beta = np.genfromtxt(dirs[1] + 'gen_' + str(gen) + '_beta_mw.csv', delimiter=',')
        v_lim = [0.94, 1.06]
        # v_lim = [-0.06, 0.06]
        q_max = q_max_gens[gen]
        color = colors[gen]

        plot_droop_curve(ax, gen, v_lim, q_max, s, beta, color)
        plot_q_of_v_scatter(ax, gen, v, q, v_lim, q_max, color)

    ax.set_xlabel("Voltage [p.u.]")
    ax.set_ylabel("Reactive Power [kVAr]")
    ax.set_ylim(-1.2, 1.2)
    ax.legend()

    plt.savefig('controls/ml_droop/droop_curves/ml_droop_curves.pdf')
    fig.show()

def plot_standard_droop():
    s = [0.95, 0.98, 1.02, 1.05]
    beta = [0, -2.5, 2.5, -2.5, 2.5]

    figsize = (12, 7)
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    colors = ["tab:blue"]

    for gen in gens:

        s = np.genfromtxt(dirs[1] + 'gen_' + str(gen) + '_s.csv', delimiter=',')
        beta = np.genfromtxt(dirs[1] + 'gen_' + str(gen) + '_beta_mw.csv', delimiter=',')
        v_lim = [0.94, 1.06]
        # v_lim = [-0.06, 0.06]
        q_max = 
        color = colors[gen]

        plot_droop_curve(ax, gen, v_lim, q_max, s, beta, color)
        plot_q_of_v_scatter(ax, gen, v, q, v_lim, q_max, color)

    ax.set_xlabel("Voltage [p.u.]")
    ax.set_ylabel("Reactive Power [kVAr]")
    ax.set_ylim(-1.2, 1.2)
    ax.legend()

    plt.savefig('controls/ml_droop/droop_curves/ml_droop_curves.pdf')
    fig.show()
