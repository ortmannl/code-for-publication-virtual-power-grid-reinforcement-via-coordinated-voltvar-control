from controls.ml_droop import Learn_Droop_Curves
from controls.ml_droop import Plot_Droop_Curves
import Simulation_Settings as params
import numpy as np
import time
import pandas as pd
import datetime
from os.path import exists
import multiprocessing as mp


def learn_droop_curves_parallel(arguments):
    if __name__ == '__main__':
        # PARALLEL PROCESSING
        # Step 1: Init multiprocessing.Pool()
        print("CPU-count: " + str(mp.cpu_count()))
        pool = mp.Pool(mp.cpu_count())
        # measure time
        start = time.time()
        print("Start parallel processing")
        # Step 2: `pool.apply`
        pool.starmap_async(Learn_Droop_Curves.learn_and_save, arguments)
        # Step 3: Don't forget to close
        pool.close()
        pool.join()
        # measure time:
        end = time.time()
        print("Parallel learning of droop curves took {} min.".format(round((end - start) / 60, 2)))


algo_params_per_gen = {
    0: {
        'max_iter': 20,
        'algo_tol': 1e-5,
        'n_s': 7,
        's_0': np.linspace(0.95, 1.05, 7),
        'n_points_boundaries': 1000
    },
    1: {
        'max_iter': 20,
        'algo_tol': 1e-5,
        'n_s': 8,
        's_0': np.linspace(0.95, 1.05, 8),
        'n_points_boundaries': 1000
    },
    2: {
        'max_iter': 20,
        'algo_tol': 1e-5,
        'n_s': 8,
        's_0': np.linspace(0.95, 1.05, 8),
        'n_points_boundaries': 1000
    },
    3: {
        'max_iter': 20,
        'algo_tol': 1e-5,
        'n_s': 8,
        's_0': np.linspace(0.95, 1.05, 8),
        'n_points_boundaries': 1000
    },
    4: {
        'max_iter': 20,
        'algo_tol': 1e-5,
        'n_s': 8,
        's_0': np.linspace(0.95, 1.05, 7),
        'n_points_boundaries': 1000
    }
}

# sample from whole year of OPF results (hourly)
pv_scenario = '2035'
sampling = 60
start_date = datetime.date(month=1, day=1, year=2018)
end_date = datetime.date(month=12, day=31, year=2018)

training_data_dir = f"simulation_results/opf/{pv_scenario}/"
result_dir = "controls/ml_droop/droop_curves/"
dirs = (training_data_dir, result_dir)

v = []
q = []
missing_files = 0

for date in pd.date_range(start_date, end_date):
    if exists(f'{training_data_dir}{pv_scenario}_opf_v_{date.month}-{date.day}.csv'):
        v_day = np.genfromtxt(f'{training_data_dir}{pv_scenario}_opf_v_{date.month}-{date.day}.csv', delimiter=',')
        v.append(v_day[params.load_bus_idx, ::sampling])
        # print('min-v:', np.min(v), '/ max-v:', np.max(v))
        q_day = np.genfromtxt(f'{training_data_dir}{pv_scenario}_opf_q_{date.month}-{date.day}.csv', delimiter=',')
        q.append(q_day[:, ::sampling])
    else:
        print("Missing OPF results:", date)
        missing_files += 1
        continue

v = np.concatenate(v, axis=1)
q = np.concatenate(q, axis=1)

# generator VAr limits
q_max_gens = params.var_limits[pv_scenario]
# gens = [0, 1, 2, 3, 4]
# for gen in gens:
#     q_max = q_max_gens[gen]
#     q[gen, :] = np.where(q[gen, :] <= q_max, q[gen, :], q_max)
#     q[gen, :] = np.where(q[gen, :] >= -q_max, q[gen, :], -q_max)

# test with synthetic OPF data
# q_max = q_max_gens[0]
# n_points = 200
# v = np.linspace(0.93, 1.07, n_points)
# q = np.zeros(n_points)
# for i in range(n_points):
#     if v[i] <= 0.95:
#         q[i] = q_max
#     elif 0.95 < v[i] < 0.99:
#         q[i] = (1-(v[i]-0.95)/(0.99-0.95))*q_max
#     elif 0.99 <= v[i] <= 1.01:
#         q[i] = 0.0
#     elif 1.01 < v[i] < 1.05:
#         q[i] = (v[i] - 1.01) / (1.05 - 1.01) * (-q_max)
#     else:
#         q[i] = -q_max

# choose which gens to handle
plot_gens = [0]
# learn_gens = [0, 1, 2, 3]
# plot_gens = [0, 1, 2]
learn_gens = [0]

learn = 0
plot = 1

if learn:
    # parallel learning
    # arguments = []
    # for gen in learn_gens:
    #     arguments.append(([gen], q_max_gens, v, q, dirs, algo_params_per_gen))
    # learn_droop_curves_parallel(arguments)
    # sequential learning
    Learn_Droop_Curves.learn_and_save(learn_gens, q_max_gens, v, q, dirs, algo_params_per_gen)

if plot:
    # for gen in plot_gens:
    #     Plot_Droop_Curves.plot_scatter_and_droop_curves([gen], v, q, q_max_gens, dirs, algo_params_per_gen)
    Plot_Droop_Curves.plot_scatter_and_droop_curves(plot_gens, v, q, q_max_gens, dirs, algo_params_per_gen)