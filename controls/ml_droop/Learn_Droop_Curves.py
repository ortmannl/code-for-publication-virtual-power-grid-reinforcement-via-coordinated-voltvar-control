from scipy.optimize import minimize, LinearConstraint
import time
import numpy as np
import Simulation_Settings as params


def I(v_t, s_i_k):
    if v_t > s_i_k:
        return 1
    else:
        return 0


def RSS_fun(min_args, s_i, x, v, n_s, beta0, x_tilde0):

    # print("min_args:", min_args)
    beta, gamma = min_args[:n_s], min_args[-n_s:]
    # print(len(beta), len(gamma))
    assert(len(beta) == n_s and len(gamma) == n_s)
    x_tilde = np.repeat(x_tilde0, len(x)) + beta0 * v
    for t in range(len(v)):
        x_tilde[t] += np.sum(beta * (v[t] - s_i) * (v[t] > s_i) - gamma * (v[t] > s_i))
    RSS = np.sum((x - x_tilde) ** 2)
    RSS += 100 * np.sum(gamma**2)
    return RSS


def learn_and_save(gens, q_max_gens, v_gens_t, q_gens_t, dirs, algo_params):

    start = time.time()

    for gen in gens:

        n_s = algo_params[gen]['n_s']
        max_iter = algo_params[gen]['max_iter']
        algo_tol = algo_params[gen]['algo_tol']
        n_points_boundaries = algo_params[gen]['n_points_boundaries']

        q_max = q_max_gens[gen]

        print(f"Learning Droop Curve of SGen {gen}...")
        print("n_s:", n_s)
        print("q_max:", q_max)

        # --- constraints --- #

        # slopes not steeper than max_slope and monotonically decreasing
        max_slope = params.maximum_droop_slope / q_max
        A1 = np.block([np.tril(np.ones((n_s, n_s))), np.zeros((n_s, n_s))])
        # print(A1)
        lb1 = np.repeat(-max_slope, n_s)
        ub1 = np.repeat(1.0, n_s)
        assert A1.shape[0] == len(lb1) == len(ub1)
        constraint1 = LinearConstraint(A1, lb1, ub1)

        # left and right hand side of droop curve must be flat (left already enforced by beta0 = 0)
        A2 = np.atleast_2d(np.concatenate((np.ones(n_s), np.zeros(n_s))))
        # print(A2)
        lb2 = np.zeros(1)
        ub2 = np.zeros(1)
        assert A2.shape[0] == len(lb2) == len(ub2)
        constraint2 = LinearConstraint(A2, lb2, ub2)

        constraints = (constraint1)     #, constraint2)

        # --- Algorithm start --- #

        # 1)
        s_i = np.linspace(0.95, 1.05, n_s)
        i = 1
        RSS = [1000]
        assert len(s_i) == n_s

        # gamma vector goes from k=[1, n_s]
        gamma = 0.01 * np.ones(n_s)
        # beta vector goes from k=[1, n_s]
        beta0 = 0
        beta = -np.ones(n_s)
        # beta = np.repeat([-max_slope, +max_slope], n_s//2)
        # beta = np.array([-max_slope, max_slope, -max_slope, max_slope])
        assert len(beta) == n_s
        x_tilde0 = 1.0

        # v = v_gens_t
        # x = q_gens_t
        v = v_gens_t[gen, :]
        x = q_gens_t[gen, :] / q_max

        # add synthetic points at (v_min, q_min) and (v_max, q_max)
        if n_points_boundaries > 0:
            v = np.concatenate((v, np.linspace(0.90, np.amin(v), n_points_boundaries), np.linspace(np.amax(v), 1.1, n_points_boundaries)))
            x = np.concatenate((x, np.ones(n_points_boundaries), -np.ones(n_points_boundaries)))
            # x = np.concatenate((x, np.repeat(q_max, n_points_boundaries), np.repeat(-q_max, n_points_boundaries)))

        while True:
            print("SGen " + str(gen) + " Iteration:", i)

            # 2)
            min_res = minimize(RSS_fun, x0=np.concatenate((beta, gamma)), args=(s_i, x, v, n_s, beta0, x_tilde0),
                               constraints=constraints)

            beta, gamma = min_res.x[:n_s], min_res.x[-n_s:]
            RSS.append(min_res.fun)

            print("beta0", beta0)
            print("beta", beta)
            print("s_i", s_i)
            print("gamma", gamma)

            # 3)
            for k in range(n_s):
                s_i[k] = gamma[k] / beta[k] + s_i[k]
                # s_i[k] = min(max(0.95, s_i[k]), 1.05)

            # 4)
            RSS_converge = abs(RSS[i] - RSS[i-1])
            print("RSS convergence:", RSS_converge)
            # print(s_i)
            if i > max_iter or RSS_converge < algo_tol or RSS_converge > 1000 or np.isnan(RSS_converge):
                break
            i += 1

        # save learned vectors
        print("Saving learned gen.")
        np.savetxt(dirs[1] + 'gen_' + str(gen) + '_s.csv', s_i, delimiter=',')
        np.savetxt(dirs[1] + 'gen_' + str(gen) + '_beta_mw.csv', beta, delimiter=',')

    end = time.time()
    print("This took", end-start, "seconds.")
