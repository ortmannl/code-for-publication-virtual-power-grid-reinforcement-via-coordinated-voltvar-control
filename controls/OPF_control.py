from pandapower import runopp, runpp


def apply_opf(net, var_limits):

    # no flexibility w.r.t. to active power of SGens -> min = max = current power production -> no curtailment
    net.sgen.min_p_mw = net.sgen.p_mw
    net.sgen.max_p_mw = net.sgen.p_mw

    v = net.res_bus.vm_pu[2:]  # remember current bus voltages

    # run the optimal power flow calculation
    try:
        runopp(net, delta=1e-10, suppress_warnings=True,
               OPF_VIOLATION=1e-10, PDIPM_COSTTOL=1e-10, PDIPM_GRADTOL=1e-10,
               PDIPM_COMPTOL=1e-10, PDIPM_FEASTOL=1e-10, PDIPM_MAX_IT=5000)
    except Exception as e:     # most likely: power flow did not converge
        # print(e, "-> Allow constraint violations for this timestep.")
        if v.max() > 1.0:
            # persistent overvoltage
            net.sgen.q_mvar = -var_limits
        elif v.min() < 1.0:
            # persistent undervoltage
            net.sgen.q_mvar = var_limits

        runpp(net)
