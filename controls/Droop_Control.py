def droop_func(v, var_limit, v_droop):

    v1, v2, v3, v4 = v_droop
    # q_min and q_max custom value:
    q_min = -var_limit
    q_max = var_limit

    if v < v1:
        return q_max
    elif v1 <= v < v2:
        return q_max * (v2-v)/(v2-v1)
    elif v2 <= v < v3:
        return 0
    elif v3 <= v < v4:
        return q_min * (v-v3)/(v4-v3)
    elif v4 <= v:
        return q_min
    else:
        raise Exception("None of the cases of the droop function could be applied")


def apply_droop(net, var_limits):

    for i in net.sgen.index:
        # bus to which sgen i is connected
        bus = net.sgen.bus[i]
        # v_m at bus to which sgen i is connected
        v_m = net.res_bus.at[bus, 'vm_pu']
        # calculate injected/consumed q_mvar according to droop control function
        q_mvar = droop_func(v_m, var_limits[i], [0.95, 0.99, 1.01, 1.05])
        # apply calculated q_mvar to net
        net.sgen.loc[i, 'q_mvar'] = q_mvar

