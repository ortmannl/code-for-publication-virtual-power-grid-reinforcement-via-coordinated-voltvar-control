from simulate import Run_Time_Series as rts
import Plot_Results as pltres
from grid import Prepare_CIGRE_Residential as cigre
import Simulation_Settings as params
import numpy as np
from os.path import exists


# control method
controls = ['none', 'droop', 'ml-droop', 'fo', 'opf']  # ['none', 'droop', 'ml-droop', 'fo', 'opf']
generation_factor = 3.5     # corresponds to PV scenario '2035'

save_plot = True

ofo_steps = 30

use_prior_simulation_results = {
    "none": True,
    "droop": True,
    "ml-droop": True,
    "fo": True,
    "opf": True
}

# prepare CIGRE LV grid (European, only residential part)
net = cigre.prepare_european_cigre_lv_residential(load_buses=params.load_bus_configuration)

# ramping
n_steps = 200
n_loads = len(net.load.index)
loads_t = np.zeros((n_steps, n_loads))
generation_t = np.zeros((n_steps, n_loads))

var_limits = np.zeros(n_loads)

for i in range(n_loads):

    nominal_load = params.nominal_loads[net.load.name[i][-3:]]
    min_generation = nominal_load / 0.95                # MW
    max_generation = nominal_load * generation_factor   # corresponds to PV scenario '2035'

    var_limits[i] = max_generation * 0.3
    loads_t[:, i] = nominal_load
    # generation_t[:, i] = np.repeat(0.65/5, n_steps)
    generation_t[:, i] = np.linspace(min_generation, max_generation, n_steps)

net.sgen.min_q_mvar = -var_limits
net.sgen.max_q_mvar = var_limits

# results
v = {}
q = {}
t = np.arange(0, n_steps, step=1)

for control in controls:

    if use_prior_simulation_results[control] and exists(f'simulation_results/virtual_reinforcement/{control}_v.csv'):
        v[control] = np.genfromtxt(f'simulation_results/virtual_reinforcement/{control}_v.csv', delimiter=',')
        print(f"Re-used simulation results for {control} control.")
    else:
        print(f"Run the virtual reinforcement simulation using {control} control...")
        if control == "fo":
            n_steps_ofo = n_steps * ofo_steps
            t_ofo = np.arange(0, n_steps_ofo, step=1)
            generation_t_ofo = np.repeat(generation_t, ofo_steps, axis=0)
            loads_t_ofo = np.repeat(loads_t, ofo_steps, axis=0)
            _, v_buses, p_loads, q_loads, q_gens, _ = rts.run_time_series(net, t_ofo, loads_t_ofo, generation_t_ofo,
                                                                        control, var_limits)
            v[control] = v_buses[:, ofo_steps-1::ofo_steps]

        else:
            _, v_buses, p_loads, q_loads, q_gens, _ = rts.run_time_series(net, t, loads_t, generation_t,
                                                                        control, var_limits)
            v[control] = v_buses[:, :]
        # save for later use
        np.savetxt(f'simulation_results/virtual_reinforcement/{control}_v.csv', v[control], delimiter=',')

total_generation_kW = 1000 * np.sum(generation_t[:, :], axis=1)
pltres.plot_virtual_reinforcement(v, total_generation_kW, controls, save=save_plot)
from Paper.Code.simulate import Run_Time_Series as rts
import Plot_Results as pltres
import grid.Prepare_CIGRE_Residential as cigre
import Simulation_Settings as params
import numpy as np


# control method
controls = ['none', 'droop', 'ml-droop', 'fo', 'opf']  # ['none', 'droop', 'ml-droop', 'fo', 'opf']
generation_factor = 3.5     # corresponds to PV scenario '2035'

# use_prior_simulation_results = {'none': True, 'droop': True, 'ml-droop': True, 'fo': True, 'opf': True}
save_plots = True

# prepare CIGRE LV grid (European, only residential part)
net = cigre.prepare_european_cigre_lv_residential(load_buses=params.load_bus_configuration)

# ramping
n_steps = 200
n_loads = len(net.load.index)
loads_t = np.zeros((n_steps, n_loads))
generation_t = np.zeros((n_steps, n_loads))

var_limits = np.zeros(n_loads)

for i in range(n_loads):

    nominal_load = params.nominal_loads[net.load.name[i][-3:]]
    min_generation = nominal_load / 0.95                # MW
    max_generation = nominal_load * generation_factor   # corresponds to PV scenario '2035'

    var_limits[i] = max_generation * 0.3
    loads_t[:, i] = nominal_load
    generation_t[:, i] = np.linspace(min_generation, max_generation, n_steps)

net.sgen.min_q_mvar = -var_limits
net.sgen.max_q_mvar = var_limits

# results
v = {}
q = {}
t = np.arange(0, n_steps, step=1)

for control in controls:

    print(f"Run the virtual reinforcement simulation using {control} control...")
    _, v_buses, p_loads, q_loads, q_gens, _ = rts.run_time_series(net, t, loads_t, generation_t, control, var_limits, animate=False)

    v[control] = v_buses[:, 1:]
    q[control] = q_gens[:, 1:]

total_generation_kW = 1000 * np.sum(generation_t[1:, :], axis=1)
pltres.plot_virtual_reinforcement(v, total_generation_kW, controls, save_plots)



