from Paper.Code.simulate import One_Day_Simulation as oneday
import Plot_Results as pltres
import grid.Prepare_CIGRE_Residential as cigre

import numpy as np
import datetime
import time
import multiprocess as mp
import calendar


def run_multi_day(net, info):

    pv_scenario = info['pv_scenario']
    control = info['control']
    start_date = info['start_date']
    n_days = info['n_days']
    alpha = info['alpha']
    res = info['res']
    location = info['location']
    n_loads_per_bus = info['n_loads_per_bus']
    load_scaling = info['load_scaling']
    pv_scaling = info['pv_scaling']
    month = str(start_date.month)

    print("Start evaluation for " + pv_scenario + ", " + control + ", month: " + month)

    # initialize results
    # voltage histogram
    n_histo = 150
    histo_range = [0.93, 1.12]

    histo_step = (histo_range[1] - histo_range[0]) / n_histo
    steps_left = (1.05 - histo_range[0]) // histo_step
    # print(steps_left)
    steps_right = (histo_range[1] - 1.05) // histo_step
    # print(steps_right)
    histo_values = np.linspace(1.05 + histo_step / 2 - steps_left * histo_step,
                               1.05 + histo_step / 2 + steps_right * histo_step,
                               n_histo)
    value_step = histo_values[1] - histo_values[0]

    voltage_histo = np.zeros(n_histo)
    max_voltages = np.zeros(n_days)
    min_voltages = np.zeros(n_days)
    q_injected = np.zeros(n_days)
    q_consumed = np.zeros(n_days)
    occurrences_overvoltage = np.zeros(n_days)
    occurrences_undervoltage = np.zeros(n_days)
    q_injected_by_bus = np.zeros((len(net.sgen.index), n_days))
    q_consumed_by_bus = np.zeros((len(net.sgen.index), n_days))

    for i in range(n_days):

        date = start_date + datetime.timedelta(days=i)
        print(date)

        t, load_data, gen_data, vm_buses, q_gens, i_loading = oneday.one_day_simulation(net, control, alpha,
                                                                                        res, date, location,
                                                                                        n_loads_per_bus,
                                                                                        load_scaling, pv_scaling)

        np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_v_' + str(date) + '.csv', vm_buses, delimiter=',')
        np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_q_' + str(date) + '.csv', q_gens, delimiter=',')

        # compute metrics
        daily_metrics = Metrics.calculate_time_series_metrics(net, vm_buses, q_gens, res, control)

        # save results
        max_voltages[i] = daily_metrics['all'].iloc[7]
        min_voltages[i] = daily_metrics['all'].iloc[8]
        q_injected[i] = daily_metrics['all'].iloc[1]
        q_consumed[i] = daily_metrics['all'].iloc[2]
        q_injected_by_bus[:, i] = daily_metrics[net.bus.name.iloc[net.load.bus]].iloc[1]
        q_consumed_by_bus[:, i] = daily_metrics[net.bus.name.iloc[net.load.bus]].iloc[2]
        occurrences_overvoltage[i] = daily_metrics['all'].iloc[9]
        occurrences_undervoltage[i] = daily_metrics['all'].iloc[10]

        # voltage histogram
        for r in range(n_histo):
            voltage_histo[r] += np.count_nonzero(np.logical_and(histo_values[r] - value_step / 2 < vm_buses,
                                                                vm_buses <= histo_values[r] + value_step / 2))

    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_voltage-histo.csv', voltage_histo,
               delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_max-voltages.csv', max_voltages,
               delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_min-voltages.csv', min_voltages,
               delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-injected.csv', q_injected,
               delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-consumed.csv', q_consumed,
               delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-injected-by-bus.csv',
               q_injected_by_bus, delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-consumed-by-bus.csv',
               q_consumed_by_bus, delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_occurrences-overvoltage.csv',
               occurrences_overvoltage, delimiter=',')
    np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_' + month + '_occurrences-undervoltage.csv',
               occurrences_undervoltage, delimiter=",")


def compute_multiday_results(arguments):
    if __name__ == '__main__':
        # PARALLEL PROCESSING
        # Step 1: Init multiprocessing.Pool()
        print("CPU-count: " + str(mp.cpu_count()))
        # pool = mp.Pool(mp.cpu_count())
        pool = mp.Pool(mp.cpu_count())  # according to number of combined scenarios
        # measure time
        start = time.time()
        print("Start parallel processing")

        # Step 2: `pool.apply`
        pool.starmap_async(run_multi_day, arguments)
        # Step 3: Don't forget to close
        pool.close()
        pool.join()
        # measure time:
        end = time.time()
        print("Simulation of the multi-day time series took {} min.".format(round((end - start) / 60, 2)))


# scenario for PV integration
pv_scenarios = ['2020', '2030', '2035']
pv_factors = {'2020': 1.0, '2030': 2.5, '2035': 3.5}

# control method
# controls = ['none', 'droop', 'fo']
controls = ['opf']
alpha = 5.0  # relevant only for FO control

# data
res = '1min'
location = 'austin'
# year is chosen later according to location to match available data
# start_date = datetime.date(month=2, day=3, year=2018)
# end_date = datetime.date(month=12, day=31, year=2018)
# n_days = (end_date - start_date).days + 1

# grid
load_bus_configuration = 'original'  # keeps the number of load buses at 5 as in the original CIGRE documentation
n_loads_per_bus = {'R11': 3, 'R15': 4, 'R16': 5, 'R17': 6, 'R18': 7}  # vary the number of loads/generation connected
# to each load bus to match CIGRE documentation
# prepare CIGRE LV grid (European, only residential part)
net = cigre.prepare_european_cigre_lv_residential(load_buses=load_bus_configuration)
# calculate H matrix and save to H.csv
# H_matrix.implicit_linearization_bolognani(net)

# scaling
load_scaling = 2.0  # scale load profiles to match CIGRE documentation also in absolute terms

arguments = []
for month in range(1, 13):
    start_date = datetime.date(month=month, day=1, year=2018)
    n_days = calendar.monthrange(2018, month)[1]

    for pv_scenario in pv_scenarios:
        # scale generation profiles according to load scaling and PV scenario
        pv_scaling = load_scaling * pv_factors[pv_scenario]
        for control in controls:
            info = {'control': control, 'alpha': alpha, 'res': res, 'start_date': start_date, 'n_days': n_days,
                    'location': location, 'pv_scenario': pv_scenario, 'load_scaling': load_scaling,
                    'pv_scaling': pv_scaling, 'n_loads_per_bus': n_loads_per_bus}
            arguments.append((net, info))

# create arguments manually:
# arguments = []
# for missing in [('2020', 'droop', 1), ('2020', 'fo', 1), ('2020', 'none', 1),
#                 ('2030', 'droop', 1), ('2030', 'fo', 1), ('2030', 'none', 1),
#                 ('2040', 'droop', 1), ('2040', 'fo', 1), ('2040', 'none', 1),
#                 ('2020', 'droop', 2), ('2020', 'fo', 2), ('2020', 'none', 2),
#                 ('2030', 'droop', 2), ('2030', 'fo', 2), ('2030', 'none', 2),
#                 ('2040', 'droop', 2), ('2040', 'fo', 2), ('2040', 'none', 2)]:
#     pv_scenario, control, month = missing
#     start_date = datetime.date(month=month, day=1, year=2018)
#     n_days = calendar.monthrange(2018, month)[1]
#     pv_scaling = load_scaling * pv_factors[pv_scenario]
#     info = {'control': control, 'alpha': alpha, 'res': res, 'start_date': start_date, 'n_days': n_days,
#             'location': location, 'pv_scenario': pv_scenario, 'load_scaling': load_scaling,
#             'pv_scaling': pv_scaling, 'n_loads_per_bus': n_loads_per_bus}
#     arguments.append((net, info))


compute_results = True
if compute_results:
    compute_multiday_results(arguments)

combine_months = False
if combine_months:

    for i, pv_scenario in enumerate(pv_scenarios):
        for j, control in enumerate(controls):
            voltage_histo = np.zeros(150)
            max_voltages = np.array([])
            min_voltages = np.array([])
            q_injected = np.array([])
            q_consumed = np.array([])
            q_injected_by_bus = np.zeros((5, 0))
            q_consumed_by_bus = np.zeros((5, 0))
            occurrences_overvoltage = np.array([])
            occurrences_undervoltage = np.array([])
            for month in range(1, 13):
                month = str(month)
                voltage_histo += np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_voltage-histo.csv',
                    delimiter=',')
                max_voltages = np.append(max_voltages, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_max-voltages.csv',
                    delimiter=','))
                min_voltages = np.append(min_voltages, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_min-voltages.csv',
                    delimiter=','))
                q_injected = np.append(q_injected, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-injected.csv',
                    delimiter=','))
                q_consumed = np.append(q_consumed, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-consumed.csv',
                    delimiter=','))
                q_injected_by_bus = np.concatenate((q_injected_by_bus, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-injected-by-bus.csv',
                    delimiter=',')), axis=1)
                q_consumed_by_bus = np.concatenate((q_consumed_by_bus, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_q-consumed-by-bus.csv',
                    delimiter=',')), axis=1)
                occurrences_overvoltage = np.append(occurrences_overvoltage, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_occurrences-overvoltage.csv',
                    delimiter=','))
                occurrences_undervoltage = np.append(occurrences_undervoltage, np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_' + month + '_occurrences-undervoltage.csv',
                    delimiter=','))

            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_voltage-histo.csv',
                       voltage_histo, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_max-voltages.csv',
                       max_voltages, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_min-voltages.csv',
                       min_voltages, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_q-injected.csv',
                       q_injected, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_q-consumed.csv',
                       q_consumed, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_q-injected-by-bus.csv',
                       q_injected_by_bus, delimiter=',')
            np.savetxt('results-statistical/' + pv_scenario + '_' + control + '_q-consumed-by-bus.csv',
                       q_consumed_by_bus, delimiter=',')
            np.savetxt(
                'results-statistical/' + pv_scenario + '_' + control + '_occurrences-overvoltage.csv',
                occurrences_overvoltage, delimiter=',')
            np.savetxt(
                'results-statistical/' + pv_scenario + '_' + control + '_occurrences-undervoltage.csv',
                occurrences_undervoltage, delimiter=",")

plot_results = False
if plot_results:

    voltage_histo = np.zeros((9, 150))
    max_voltages = np.zeros((9, 365))
    min_voltages = np.zeros((9, 365))
    q_injected = np.zeros((9, 365))
    q_consumed = np.zeros((9, 365))
    occurrences_overvoltage = np.zeros((9, 365))
    occurrences_undervoltage = np.zeros((9, 365))

    for i, pv_scenario in enumerate(pv_scenarios):
        for j, control in enumerate(controls):
            voltage_histo[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_voltage-histo.csv', delimiter=',')
            max_voltages[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_max-voltages.csv', delimiter=',')
            min_voltages[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_min-voltages.csv', delimiter=',')
            q_injected[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_q-injected.csv', delimiter=',')
            q_consumed[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_q-consumed.csv', delimiter=',')
            occurrences_overvoltage[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_occurrences-overvoltage.csv', delimiter=',')
            occurrences_undervoltage[i * len(controls) + j, :] = np.genfromtxt(
                'results-statistical/' + pv_scenario + '_' + control + '_occurrences-undervoltage.csv', delimiter=",")

    for i, pv_scenario in enumerate(pv_scenarios):
        # v_max and v_min
        pltres.plot_vmax_vmin_year(max_voltages[i * len(controls):(i + 1) * len(controls), :],
                                   min_voltages[i * len(controls):(i + 1) * len(controls), :],
                                   location, pv_scenario, save=True)
        # total Q
        pltres.plot_total_q_weekly(q_injected[i * len(controls):(i + 1) * len(controls), :],
                                   q_consumed[i * len(controls):(i + 1) * len(controls), :],
                                   location, pv_scenario, style='separated-bars', save=True)

        # voltage histos
        for j, control in enumerate(controls):
            pltres.plot_histogram_voltages_year(voltage_histo[i * len(controls) + j, :], location, control, pv_scenario,
                                                save=True)
            # q by bus
            if control == 'none':
                continue
            else:
                q_injected_by_bus = np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_q-injected-by-bus.csv', delimiter=',')
                q_consumed_by_bus = np.genfromtxt(
                    'results-statistical/' + pv_scenario + '_' + control + '_q-consumed-by-bus.csv', delimiter=',')
                pltres.plot_q_by_bus(net, np.sum(q_injected_by_bus, axis=1), np.sum(q_consumed_by_bus, axis=1),
                                     location, control, pv_scenario, save=True)

    pltres.plot_comparison_total_q_year(location, q_injected, q_consumed, save=True)