# Collection of general plotting functions
# plot data, time series results, metrics, etc.

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cmap
from matplotlib.ticker import ScalarFormatter
import matplotlib.gridspec as gridspec
import Simulation_Settings as params
import seaborn as sns

plt.rcParams.update({'font.family': 'serif'})
plt.rcParams.update({'font.sans-serif': 'Times New Roman'})
plt.rcParams.update({'mathtext.fontset': 'dejavuserif'})

FONTSCALE = 1.2

plt.rc('font', size=12*FONTSCALE)          # controls default text sizes
plt.rc('axes', titlesize=15*FONTSCALE)     # fontsize of the axes title
plt.rc('axes', labelsize=13*FONTSCALE)     # fontsize of the x and y labels
plt.rc('xtick', labelsize=12*FONTSCALE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=12*FONTSCALE)    # fontsize of the tick labels
plt.rc('legend', fontsize=11*FONTSCALE)    # legend fontsize
plt.rc('figure', titlesize=15*FONTSCALE)   # fontsize of the figure title
suptitlesize = 20*FONTSCALE

PADDING = {
    'w_pad': 1.0,
    'h_pad': 1.0
}

DISPLAY_NAMES = {
    'none': "Uncontrolled",
    'droop': "Droop",
    'ml-droop': "ML Droop",
    'fo': "OFO",
    'opf': "ORPF"
}

COLORS = ["tab:orange", "tab:blue", "tab:green", "tab:purple", "tab:red", "tab:brown"]
LINESTYLES = ["-.", "-", "-.", "-", "-.", "-"]


def plot_data(net, t, load, gen, pv_scenario, highlight=None, mode='standard', save=False):
    """plot generation and consumption in a figure containing to subplots"""


    n_steps = len(t)

    # create subplots
    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(9, 10))

    # ax1: plot active power consumption at all buses
    # ax1.set_title('Total Consumption at each Bus (kW)')
    ax1.plot(t, 1000 * load, linewidth=2)
    # ax1.plot(t, np.average(1000 * load, axis=1), linewidth=1.5)
    ax1.set_xlim(0, n_steps)
    ax1.set_xticks([n_steps // 12 * s for s in range(0, 13)])
    ax1.set_xticklabels([2*i for i in range(0, 13)])
    ax1.set_ylabel('Load [kW]')
    ax1.set_xlabel('Time [h]')
    ax1.legend([name[-3:] for name in net.load.name], loc='right')

    # ax2: plot active power generation at all buses
    # ax2.set_title('Total Generation at each Bus (kW)')
    ax2.plot(t, 1000 * gen[:, :], linewidth=2)
    # ax2.plot(t, np.average(1000 * gen, axis=1), linewidth=1.5)
    ax2.set_xlim(0, n_steps)
    ax2.set_xticks([n_steps // 12 * s for s in range(0, 13)])
    ax2.set_xticklabels([2*i for i in range(0, 13)])
    ax2.set_ylabel('Generation [kW]')
    ax2.set_xlabel('Time [h]')
    ax2.legend([name[-3:] for name in net.sgen.name], loc='right')

    ax1.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5)
    ax2.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/' + pv_scenario + '_data.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.show()


def plot_v_q(net, t, vm_buses, q_gens, pv_scenario, control, highlight=None, mode='standard', save=False):
    """Plot voltages at buses and the reactive power control"""

    location = params.location

    fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(9, 9), sharex=True)
    
    # assemble title of figure containing information about method
    # title = 'Method = ' + str(control) + ', Res = ' + res + ', varLimits = box'
    # if control == 'fo':
    #     title += r', $\alpha$ = ' + str(alpha)
    # fig.suptitle(title)
    # ax1: plot vm at all Buses
    # ax1.set_title(r'$v_{h}$ at Buses [0, R1-R18]')

    if mode == 'min-med-max':
        ax1.fill_between(t, np.min(vm_buses[:, :], axis=0), np.max(vm_buses[:, :], axis=0), facecolor='#bfbfbf')
        ax1.plot(t, np.min(vm_buses[:, :], axis=0), linewidth=0.5, color='tab:blue', zorder=3)
        ax1.plot(t, np.median(vm_buses[:, :], axis=0), linewidth=0.5, color='tab:orange', zorder=3)
        ax1.plot(t, np.max(vm_buses[:, :], axis=0), linewidth=0.5, color='tab:red', zorder=3)
        highlight = None
    else:
        ax1.plot(t, np.transpose(vm_buses[:, :]), linewidth=2, zorder=3)  # 0.5)
    ax1.set_xlim(0, 1440)
    # ax1.set_xticks([1440 // 12 * s for s in range(0, 13)])
    # ax1.set_xticklabels([2*i for i in range(0, 13)])
    # ax1.set_xlabel('Time [h]')
    ax1.set_ylabel(r'$v_{h}$ [p.u.]')
    if mode == 'min-med-max':
        ax1.legend([r'$v_{min}$', r'$v_{med}$', r'$v_{max}$'], loc='upper right')
    else:
        ax1.legend([name[-3:] for name in net.bus.name], loc='upper right')

    # ax2: plot q_mvar at all Generators
    # ax2.set_title(r'$q_{h}$ at Buses [R1-R18]')
    ax2.plot(t, np.transpose(1000 * q_gens), linewidth=2, zorder=3)   # 0.5)
    ax2.set_xlim(0, 1440)
    ax2.set_xticks([1440 // 12 * s for s in range(0, 13)])
    ax2.set_xticklabels([2*i for i in range(0, 13)])
    ax2.set_xlabel('Time [h]')
    ax2.set_ylabel(r'$q_{h}$ [kVAr]')
    ax2.legend([name[-3:] for name in net.sgen.name], loc='lower right')

    # highlight a specific bus in the voltage plot:
    if highlight is not None:
        current_color = 0
        handles, labels = [], []
        highlight_colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple"]
        for i, line in enumerate(ax1.lines):
            if i in highlight:
                line.set_color(highlight_colors[current_color])
                current_color += 1
                if current_color == len(highlight_colors):
                    current_color = 0
                handles.append(line)
                labels.append(net.bus.name[i][-3:])
            else:
                line.set_color('gray')
                line.set_alpha(0)
        ax1.legend(handles, labels, loc='upper right')

    ax1.axhline(y=1.05, color='#999999', linestyle='--')
    ax1.axhline(y=0.95, color='#999999', linestyle='--')

    ax1.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5)
    ax2.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/' + pv_scenario + '_' + control + '_' + 'voltages-var.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)

    plt.close()


def plot_line_loading(net, t, i_loading, info, save=False):

    control = info['control']
    location = params.location
    pv_scenario = info['pv_scenario']

    n_steps = len(t)

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    # fig.suptitle('Line Loading')

    # ax: plot loading percent of all lines
    ax.plot(t, np.transpose(i_loading[:, :]), linewidth=0.5)
    ax.axhline(y=100, color='gray', linestyle='--')
    ax.set_xlim(0, n_steps)
    ax.set_ylim(0, 105)
    ax.set_xticks([n_steps // 24 * s for s in range(0, 25)])
    ax.set_xticklabels(list(range(0, 25)))
    ax.set_xlabel('Time [h]')
    ax.set_ylabel('Line loading [%]')
    ax.legend([name[5:] for name in net.line.name], loc='right')

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/' + location + '_' + pv_scenario + '_' + control + '_' + 'line-loading.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)

    plt.show()


def plot_metrics_bar(metrics, voltages, controls, pv_scenario, save=False):

    cm_power = cmap.get_cmap('Greens')
    bar_colors_power = cm_power(np.linspace(0.4, 0.9, len(controls)))

    cm_voltage = cmap.get_cmap('Blues')
    bar_colors_voltage = cm_voltage(np.linspace(0.6, 0.9, len(controls)))

    cm_duration = cmap.get_cmap('Reds')
    bar_colors_duration = cm_duration(np.linspace(0.4, 0.9, len(controls)))

    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(17, 6), gridspec_kw={'width_ratios': [2, 3, 2]})
    width = 0.8
    alpha = 0.9

    # total injected/consumed Q
    ax1.set_title('Reactive power usage')
    for i, control in enumerate(controls):
        ax1.bar(DISPLAY_NAMES[control], metrics[control]['total injected Q squared (MVAr2h)'],
                width=width, color=bar_colors_power[i], zorder=3, alpha=alpha)
        ax1.bar(DISPLAY_NAMES[control], -metrics[control]['total consumed Q squared (MVAr2h)'],
                width=width, color=bar_colors_power[i], zorder=3, alpha=alpha)
    ax1.axhline(y=0.0, color='black', linestyle='-', lw=1.0)
    ax1.set_ylim(-11, 3)
    ax1.set_ylabel(r'Reactive power squared [$MVAr^2h$]')
    ax1.ticklabel_format(axis='y', style='plain', scilimits=(0, 0), useMathText=True)

    # boxplot voltages
    ax2.set_title('Bus voltages')
    # data = [np.repeat(voltage_histos[control]['x'], voltage_histos[control]['y'].astype('int') // 50) for control in controls]
    data = [voltages[control] for control in controls]
    # ax2.boxplot(x=data, labels=[DISPLAY_NAMES[control] for control in controls],
    #             vert=True, showfliers=False, whis=[1, 99], showbox=False)
    # palette = sns.color_palette("ch:start=.2,rot=-.3")
    # sns.violinplot(data=data, ax=ax2, scale='area', palette=palette, cut=0, bw='scott',
    #                saturation=1, inner=None, linewidth=0)
    # ax2.set_xticks(list(range(0, len(controls))))
    parts = ax2.violinplot(data, widths=width, vert=True, showextrema=False)
    for i, pc in enumerate(parts['bodies']):
        pc.set_zorder(3)
        pc.set_facecolor(bar_colors_voltage[i])
        pc.set_alpha(alpha)
    ax2.set_xticks(list(range(1, len(controls)+1)))
    ax2.set_xticklabels([DISPLAY_NAMES[control] for control in controls])
    ax2.axhline(y=1.0, color='black', linestyle='-', lw=1.0, zorder=-5)
    ax2.axhline(y=params.v_min, color='gray', linestyle='--', lw=1.0, zorder=-5)
    ax2.axhline(y=params.v_max, color='gray', linestyle='--', lw=1.0, zorder=-5)
    ax2.set_ylabel('Voltage [p.u.]')
    ax2.set_ylim(0.94, 1.1)
    # ax2.set_yticks(ax2.get_yticks())
    # ax2.set_yticklabels(ax2.get_yticks()+1.0)

    # occurrence of over-/undervoltages
    ax3.set_title('Constraint violations')
    for i, control in enumerate(controls):
        ax3.bar(DISPLAY_NAMES[control], metrics[control]['integral of overvoltage (Vh)'],
                width=width, color=bar_colors_duration[i], zorder=3, alpha=alpha)
        ax3.bar(DISPLAY_NAMES[control], -metrics[control]['integral of undervoltage (Vh)'],
                width=width, color=bar_colors_duration[i], zorder=3, alpha=alpha)
    ax3.axhline(y=0.0, color='black', linestyle='-', lw=1.0)
    ax3.set_ylabel('Integral of constraint violations [Vh]')
    ax3.set_ylim(-2, 40)
    # formatter = ScalarFormatter()
    # formatter.set_scientific(True)
    # formatter.set_powerlimits((0, 0))
    # formatter.set_useMathText(True)
    # ax3.yaxis.set_major_formatter(formatter)

    # amount of active power line losses
    # ax4.set_title('Line losses')
    # for i, control in enumerate(controls):
    #     ax4.bar(DISPLAY_NAMES[control], metrics[control]['total losses in grid (MWh)'],
    #             width=width, color=bar_colors_voltage[i], zorder=3, alpha=alpha)
    # ax4.axhline(y=0.0, color='black', linestyle='-', lw=1.0)
    # ax4.set_ylabel('Total losses in grid [MWh]')
    # ax4.set_ylim()

    for ax in [ax1, ax2, ax3]:
        ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5, axis='y')
        ax.set_axisbelow(True)
        ax.tick_params(axis='x', labelrotation=30)


    if not save:
        fig.suptitle(f"{pv_scenario}: Controller performance", fontsize=suptitlesize, fontweight='bold')

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/' + pv_scenario + '_' + 'metrics_bar.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.close()


def comparison_scenarios(metrics, controls, pv_scenarios, save=False):

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(14, 6))
    markers = {'none': 'd', 'droop': '^', 'ml-droop': 'v', 'fo': 'o', 'opf': '*'}
    colors = {'none': 'tab:brown', 'droop': 'tab:purple', 'ml-droop': 'tab:red', 'fo': 'tab:blue', 'opf': 'tab:orange'}

    for control in controls:
        used_var2 = [metrics[pv_scenario][control]['total injected Q (MVArh)'] +
                     metrics[pv_scenario][control]['total consumed Q (MVArh)']
                     for pv_scenario in pv_scenarios]
        total_constraint_violations = [metrics[pv_scenario][control]['integral of overvoltage (Vh)'] +
                                       metrics[pv_scenario][control]['integral of undervoltage (Vh)']
                                       for pv_scenario in pv_scenarios]
        ax1.plot(pv_scenarios, used_var2,
                 marker=markers[control], markersize=10, color=colors[control], ls='--', label=DISPLAY_NAMES[control])
        ax2.plot(pv_scenarios, total_constraint_violations,
                 marker=markers[control], markersize=10, color=colors[control], ls='--', label=DISPLAY_NAMES[control])

    for ax in [ax1, ax2]:
        ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', axis='y', linewidth=0.5, zorder=0)
        ax.legend()

    ax1.set_ylabel(r'Reactive power usage [MVArh]')
    ax2.set_ylabel('Integral of constraint violations [Vh]')

    if not save:
        fig.suptitle("Comparison of controller performance for different PV scenarios", fontsize=suptitlesize, fontweight='bold')

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/comparison_scenarios.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.show()


def plot_vmax_vmin_year(max_voltages, min_voltages, location, pv_scenario, save=False):

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    t = np.arange(0, max_voltages.shape[1], step=1)

    plt.gca().set_prop_cycle('color', ['red', 'blue', 'green'])
    ax.plot(t, np.transpose(min_voltages), linewidth=0.5)
    ax.plot(t, np.transpose(max_voltages), linewidth=0.5)
    ax.axhline(y=1.05, color='#3d3d3d', linestyle='--')
    ax.axhline(y=0.95, color='#3d3d3d', linestyle='--')
    ax.set_xlim(0, len(t))
    # ax.set_xticks([len(t) // 30.5 * s for s in range(0, 12)])
    # ax.set_xticklabels(list(('0' + str(m))[-2:] for m in range(1, 13)))
    ax.set_xlabel('Day')
    ax.set_ylabel(r'$v_{min}\ /\ v_{max}$ [p.u.]')
    ax.legend(['None', 'Droop', 'FO'])

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/statistical/' + location + '_' + pv_scenario + '_' + 'min-max-voltages.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)

    plt.show()


def plot_total_q_weekly(q_injected, q_consumed, location, pv_scenario, style='standard', save=False):

    # aggregate weeks
    q_injected_weekly = np.zeros((q_injected.shape[0], 53))
    q_consumed_weekly = np.zeros((q_injected.shape[0], 53))
    for i in range(q_injected.shape[0]):
        for j in range(365):
            week = j//7
            q_injected_weekly[i, week] += q_injected[i, j]
            q_consumed_weekly[i, week] += q_consumed[i, j]

    t = np.arange(0, q_injected_weekly.shape[1], step=1)

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    # simple lines
    if style == 'standard':
        plt.gca().set_prop_cycle('color', ['tab:blue', 'tab:green'])
        ax.plot(t, np.transpose(q_injected_weekly[1:]), linewidth=0.5)
        ax.plot(t, np.transpose(-q_consumed_weekly[1:]), linewidth=0.5)

    # separated bars
    if style == 'separated-bars':
        width = 0.5
        pos_droop = t - width/2
        pos_fo = t + width/2
        ax.bar(pos_droop, q_injected_weekly[1], width=width, color='tab:blue', label='Droop', zorder=3)
        ax.bar(pos_fo, q_injected_weekly[2], width=width, color='tab:orange', label='FO', zorder=3)
        ax.bar(pos_droop, -q_consumed_weekly[1], width=width, color='tab:blue', label='Droop', zorder=3)
        ax.bar(pos_fo, -q_consumed_weekly[2], width=width, color='tab:orange', label='FO', zorder=3)

    if style == 'stacked-bars':
        width = 0.9
        ax.bar(t, q_injected_weekly[1], width=width, color='tab:blue', label='Droop')
        ax.bar(t, q_injected_weekly[2], width=width, color='tab:orange', label='FO')
        ax.bar(t, -q_consumed_weekly[1], width=width, color='tab:blue', label='Droop')
        ax.bar(t, -q_consumed_weekly[2], width=width, color='tab:orange', label='FO')

    ax.axhline(y=0.0, color='black', linestyle='-', linewidth=0.5, label='_nolegend_')
    ax.set_xlim(0, len(t))
    # ax.set_xticks([len(t) * s for s in range(0, 12)])
    # ax.set_xticklabels(list(('0' + str(m))[-2:] for m in range(1, 13)))
    ax.set_xlabel('Week')
    ax.set_ylabel(r'$q$ [MVArh]')
    ax.legend(['Droop', 'FO'])

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5, axis='y', zorder=0)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/statistical/' + location + '_' + pv_scenario + '_' + 'total-q-weekly.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)

    plt.show()


def plot_histogram_voltages(voltage_histo, control, pv_scenario, save=False):

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    x, y = voltage_histo['x'], voltage_histo['y']

    n_histo = params.n_histo
    histo_range = params.histo_range
    value_step = x[1] - x[0]

    # assert len(x) == len(y) == n_histo

    y_feasible = np.zeros(len(y))
    y_unfeasible = np.zeros(len(y))

    for i, v in enumerate(x):
        if v < 0.95 or v > 1.05:
            y_unfeasible[i] = y[i]
        else:
            y_feasible[i] = y[i]

    # ax: plot voltage histogram for selected bus
    ax.bar(x, y_feasible/np.sum(y)*100, width=value_step * 0.8, color='tab:blue', zorder=3)
    ax.bar(x, y_unfeasible/np.sum(y)*100, width=value_step * 0.8, color='tab:red', zorder=3)
    ax.set_xlim(*histo_range)
    ax.set_ylim(0, 6)
    ax.axvline(x=1.05, color='gray', linestyle='--')
    ax.axvline(x=0.95, color='gray', linestyle='--')
    ax.set_ylabel('Fraction of Measurements [%]')
    ax.set_xlabel('Bus voltage [p.u.]')

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5, axis='y', zorder=0)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/statistical/' + '_' + pv_scenario + '_' + control + '_' + 'voltage-histo.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.show()


def plot_q_by_bus(net, q_injected_by_bus, q_consumed_by_bus, location, control, pv_scenario, save=False):

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    if control == 'droop':
        color = 'tab:blue'
    elif control == 'fo':
        color = 'tab:orange'

    x = np.arange(0, len(net.load.bus), step=1)
    total_actuation = q_injected_by_bus + q_consumed_by_bus
    ax.bar(x, total_actuation / np.sum(total_actuation) * 100, width=0.8, color=color, zorder=3)

    ax.set_ylabel('Participation in total actuation [%]')
    ax.set_xlabel('Bus')
    # ax.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0), useMathText=True)
    ax.set_xticks(x)
    ax.set_xticklabels([name[-3:] for name in net.bus.name.iloc[net.load.bus]])

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5, axis='y', zorder=0)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/statistical/' + location + '_' + pv_scenario + '_' + control + '_' + 'q-by-bus.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.show()


def plot_comparison_total_q_year(location, q_injected, q_consumed, save=False):

    total_q_injected = np.sum(q_injected, axis=1)
    total_q_consumed = np.sum(q_consumed, axis=1)

    print(total_q_consumed + total_q_injected)

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    x = np.arange(0, 3, step=1)

    ax.bar(x-0.2, total_q_injected[[1, 4, 7]], width=0.35, color='tab:blue', zorder=3)
    ax.bar(x+0.2, total_q_injected[[2, 5, 8]], width=0.35, color='tab:orange', zorder=3)
    ax.bar(x-0.2, -total_q_consumed[[1, 4, 7]], width=0.35, color='tab:blue', zorder=3)
    ax.bar(x+0.2, -total_q_consumed[[2, 5, 8]], width=0.35, color='tab:orange', zorder=3)
    ax.axhline(y=0.0, color='black', linestyle='-', linewidth=0.5, label='_nolegend_')

    ax.set_ylabel('q [MVArh]')
    ax.set_xlabel('PV integration scenario')
    # ax.ticklabel_format(axis='y', style='scientific', scilimits=(0, 0), useMathText=True)
    ax.set_xticks(x)
    ax.set_xticklabels(['2020', '2030', '2035'])
    ax.legend(['Droop', 'FO'])

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='-', linewidth=0.5, axis='y', zorder=0)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/statistical/' + location + '_' + 'comparison-total-q.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.show()


def plot_virtual_reinforcement(v, total_generation_kW, controls, save=False):

    fig, ax = plt.subplots(1, 1, figsize=(9, 5))
    
    for i, control in enumerate(controls):

        v_max = np.amax(v[control], axis=0)

        p_tot = total_generation_kW
        ax.plot(p_tot[1:], v_max[1:], color=COLORS[i], marker='', ls=LINESTYLES[i], label=DISPLAY_NAMES[control], lw=2, zorder=3)

        ax.axvline(p_tot[np.argmax(v_max > params.v_max + 0.00001)], color=COLORS[i], ls="--", alpha=0.8)

        # x = list(range(len(p_tot)))
        # ax.plot(x, v_max, color=COLORS[i], marker='', ls='-', label=DISPLAY_NAMES[control], lw=1, zorder=3)
        
    ax.axhline(params.v_min, color="grey", ls="--")
    ax.axhline(params.v_max, color="grey", ls="--")

    ax.set_ylim(bottom=0.99)
    plt.locator_params(axis='y', nbins=4)

    ax.legend()
    ax.set_xlim(p_tot.min(), p_tot.max())

    ax.set_ylabel(r"Maximum voltage $v_{max}$ [p.u.]")
    ax.set_xlabel(r"Total generation $P_{tot}$ [kW]")

    ax.grid(visible=True, which='major', color='#cfcfcf', linestyle='--', linewidth=0.5)

    fig.tight_layout(w_pad=PADDING['w_pad'], h_pad=PADDING['h_pad'])
    if save:
        plt.savefig('plots/virtual-reinforcement.pdf',
                    dpi=None,
                    facecolor='w',
                    edgecolor='w',
                    orientation='portrait',
                    format=None,
                    transparent=False,
                    bbox_inches=None,
                    pad_inches=0.1,
                    metadata=None)
    plt.close()
