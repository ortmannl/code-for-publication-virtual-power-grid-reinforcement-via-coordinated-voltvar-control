"""Use the real data from Dataport (3 locations available, 1 year of data) to run a time series for a 1 day test case
(European CIGRE LV benchmark grid)"""

from . import Run_Time_Series as rts
from data import Prepare_Data as data
import Simulation_Settings as params

import numpy as np


def one_day_simulation(net, control, pv_scenario, date):

    day = date.day
    month = date.month

    # prepare data for all load buses
    t, load_data, gen_data, date = data.prepare_1day_deterministic(net, params.location, params.res, day, month,
                                                                   params.n_loads_per_bus, params.load_scaling,
                                                                   params.pv_scaling[pv_scenario])
    # downsampling
    downsampling = 1
    start = 0
    end = len(t)
    # t = np.repeat(t[start:end:downsampling], 6)
    # load_data = np.repeat(load_data[start:end:downsampling, :], 6, axis=0)
    # gen_data = np.repeat(gen_data[start:end:downsampling, :], 6, axis=0)
    t = t[start:end:downsampling]
    load_data = load_data[start:end:downsampling, :]
    gen_data = gen_data[start:end:downsampling, :]

    # smoothen data
    smoothen = False
    if smoothen:
        load_data, gen_data = data.smoothen_data(load_data, gen_data)

    # update var limits for the controllable SGens to match scenario
    var_limits = params.var_limits[pv_scenario]
    net.sgen.min_q_mvar = -var_limits
    net.sgen.max_q_mvar = var_limits
    
    # run simulation
    t, vm_buses, p_loads, q_loads, q_gens, i_loading, p_losses = rts.run_time_series(net, t, load_data, gen_data,
                                                                           control, var_limits, animate=False)
    return t, load_data, gen_data, vm_buses, q_gens, i_loading, p_losses
