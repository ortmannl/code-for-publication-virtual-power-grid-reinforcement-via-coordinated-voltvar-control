from grid import Plot_Network as plot_net
import Animate_png_to_mp4 as anim
from controls import Droop_Control as droop, OPF_control as opf, ML_Droop_Control as mldroop, FO_control as fo
import Simulation_Settings as params

from tqdm.auto import tqdm
import matplotlib.pyplot as plt
import time
import pandapower as pp
import numpy as np
import os
from datetime import datetime



def run_time_series(net, T, loads_t, generation_t, control, var_limits, animate=False):
    """Run a time series of power flow simulations on the given network using the specified control method.

    :param net: The network on which the simulation is performed.
    :type net: pandapower-network
    :param loads_t: The active power consumption of all loads in the network over time; rows: loads, cols: time-steps.
    :type generation_t: ndarray
    :param generation_t: The active power generated at all loads and fed into the grid; rows: loads, cols: time-steps.
    :type generation_t: ndarray
    :param T: Numpy array or similar defining the time-steps of the time series simulation.
    :type T: iterable
    :param animate: if True, a video clip illustrating the evolution of the grid state will be saved to the local folder.
    :type animate: bool
    :param control: Control Method to be used for the Time Series Simulation ('none', 'fo', 'droop')
    :type control: str
    :param alpha: The feedback gain used for FO control. Does not have to be specified for all other control methods.
    :type alpha: float

    :returns: T: time-step vector
    :rtype T: iterable
    :returns res_vm_pu: voltages at all buses at all time-steps; rows: buses, cols: time-steps.
    :rtype res_vm_pu: ndarray
    :returns res_p_loads: active power consumption at all buses at all time-steps; rows: loads, cols: time-steps.
    :rtype res_p_loads: ndarray
    :returns res_q_loads: reactive power consumption at all buses at all time-steps; rows: loads, cols: time-steps.
    :rtype res_q_loads: ndarray
    :returns res_q_gens: reactive power injected/consumed by the generators at all buses at all time-steps; rows: generators, cols: time-steps.
    :rtype res_q_gens: ndarray
    """

    if control not in ['none', 'droop', 'ml-droop', 'APC', 'fo', 'opf']:
        raise ValueError("Unknown Control Method")

    if control == 'fo' and params.alpha is None:
        raise ValueError("Alpha must be defined for using FO-control")

    cos_phi = 0.95
    sin_phi = np.sin(np.arccos(cos_phi))

    n_steps = len(T)
    res_vm_pu = np.zeros((len(net.bus.index), n_steps))
    res_p_loads = np.zeros((len(net.load.index), n_steps))
    res_q_loads = np.zeros((len(net.load.index), n_steps))
    res_q_gens = np.zeros((len(net.load.index), n_steps))
    res_i_loading = np.zeros((len(net.line.index), n_steps))
    res_p_losses = np.zeros((len(net.line.index), n_steps))

    if control == 'fo':
        lambda_t = np.zeros((2 * (len(net.load.index)), len(T) + 1))

    if animate:
        # create folder for pngs
        path = os.getcwd()
        # define the name of the directory to be created (name using current time)
        date_time = datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
        path = path + "/animations/" + date_time
        try:
            os.mkdir(path)
        except OSError:
            print("Creation of the directory %s failed" % path)
        else:
            print("Successfully created the directory %s " % path)

    start = time.time()  # measure elapsed time

    # do not show progress bar if multiple threads are active
    progress_bar = False
    if progress_bar:
        iterate = enumerate(tqdm(T))
    else:
        iterate = enumerate(T)

    for i, t in iterate:

        load = loads_t[i, :]
        generation = generation_t[i, :]
        # load = generation = 0

        # keep loads in order of index
        net.load.p_mw = load * cos_phi
        net.load.q_mvar = load * sin_phi
        net.sgen.p_mw = generation

        # solve power flow for new current load and generation
        if control not in ['opf']:
            pp.runpp(net)

        # apply current control based on new state of the grid
        if control == 'opf':
            opf.apply_opf(net, var_limits)

        if control == 'droop':
            droop.apply_droop(net, var_limits)

        if control == 'ml-droop':
            if i == 0:
                q_t = np.zeros(len(net.sgen.index))
            q_t1 = mldroop.apply_droop(net, q_t, var_limits)
            q_t = q_t1  # remember q for low-pass filtering

        if control == 'fo':
            # get new lambda_t1 and set all set-points of reactive power
            lambda_t[:, i+1] = fo.apply_fo(net, lambda_t[:, i], var_limits)


        # run power flow for controlled state of the grid
        if control not in ['none', 'opf', 'fo']:
            pp.runpp(net)

        # store results of iteration
        # store vm_pu result (p.u.)
        res_vm_pu[:, i] = net.res_bus.vm_pu.to_numpy()
        # store p_load result (MW)
        res_p_loads[:, i] = net.res_load.p_mw.to_numpy()
        # store q_load result (MVAr)
        res_q_loads[:, i] = net.res_load.q_mvar.to_numpy()
        # store q_gen result (MVAr)
        res_q_gens[:, i] = net.res_sgen.q_mvar.to_numpy()
        # store i_loading result (%)
        res_i_loading[:, i] = net.res_line.loading_percent.to_numpy()
        # store line P losses result (MW)
        res_p_losses[:, i] = net.res_line.pl_mw.to_numpy()

        if animate:
            fig = plot_net.plot_network_static(net)
            plt.figure(fig.number)
            img_name = "000" + str(i)
            img_name = img_name[-4:]
            img_path = path + "/" + img_name + ".png"
            plt.savefig(img_path)
            plt.close()

    end = time.time()
    # print(f"Simulation of the time series using {control} control took {round((end - start), 2)} sec.")

    if animate:
        # call function to combine all saved pngs into movie
        anim.animate_png_mp4(path)

    return T, res_vm_pu, res_p_loads, res_q_loads, res_q_gens, res_i_loading, res_p_losses
