import os
import moviepy.video.io.ImageSequenceClip


def animate_png_mp4(path_to_folder, fps=30):
    print(os.listdir(path_to_folder))
    image_files = [path_to_folder + '/' + img for img in os.listdir(path_to_folder) if img.endswith(".png")]
    clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
    clip.write_videofile('animations/animated.mp4')
