import numpy as np
import Paper.Code.Simulation_Settings as params
from scipy.optimize import fsolve


low_pass_lambda = params.low_pass_lambda

X = np.genfromtxt('H.csv', delimiter=',')

norm_X = np.linalg.norm(X)
# beta_max = 1/norm_X
eqn = lambda beta: np.linalg.norm((1-low_pass_lambda) * np.eye(len(X)) - low_pass_lambda * beta * np.eye(len(X)) @ X) - 1

beta_max = fsolve(eqn, x0=1)

print("Norm of X/H:", norm_X)
print("Maximum Slope:", *beta_max)
