# prepare the CIGRE LV benchmark network by only using the residential part
# and by adding both loads and generation to all nodes

import pandapower.networks as nw
import pandapower as pp
from pandapower import create_poly_cost
import Simulation_Settings as params
import numpy as np

def prepare_european_cigre_lv_residential(load_buses='all'):

    # import CIGRE LV distribution network
    net = nw.create_cigre_network_lv()
    # delete all loads
    net.load = net.load.iloc[0:0]
    # only keep residential grid
    net.bus = net.bus.iloc[0:20]
    net.line = net.line.iloc[0:17]
    net.trafo = net.trafo.iloc[0:1]
    net.switch = net.switch.iloc[0:1]
    net.bus_geodata = net.bus_geodata[0:20]

    if load_buses == 'all':
        # add load and generator at each bus (apart from bus 0 and R0), R[1-18]
        loads_at = list(range(1,19))
    elif load_buses == 'original':
        # create loads/gens only at R[11, 15, 16, 17, 18]
        loads_at = [11, 15, 16, 17, 18]
    else:
        raise ValueError("List of load_buses is invalid")

    for i in loads_at:
        bus = i+1
        pp.create_load(net, bus=bus, p_mw=0, q_mvar=0, name='Load R'+str(i), controllable=False)
        # create generation and define it as controllable for OPF calculations;
        # only initalize min/max values, they will be defined approprietly later
        pp.create_sgen(net, bus=bus, p_mw=0, q_mvar=0, name='SGen R'+str(i), controllable=True,
                       min_q_mvar=0, max_q_mvar=0, min_p_mw=0, max_p_mw=0)

    # set voltage limits and cost function for OPF
    net.bus['min_vm_pu'] = np.repeat(params.v_min, len(net.bus.index))
    net.bus['max_vm_pu'] = np.repeat(params.v_max, len(net.bus.index))

    # define cost function: identity -> same cost (1 EUR) for all SGens
    for i in net.sgen.index:
        create_poly_cost(net, element=i, et="sgen", cp1_eur_per_mw=0, cp2_eur_per_mw2=0, cq1_eur_per_mvar=0, cq2_eur_per_mvar2=1)

    return net


def prepare_american_cigre_lv_residential():

    net = pp.create_empty_network(name='cigre_lv_residential_american', f_hz=60.0, sn_mva=1, add_stdtypes=True)

    # buses
    n_buses = 15
    geodata = [(0.0, 0.0), (0.0, -25.0), (0.0, -50.0), (0.0, -75.0), (0.0, -100.0),
               (-25.0, -25.0), (0.866*25.0, -12.5), (50.0, -25.0),
               (-25.0, -50.0), (0.866*25.0, -37.5), (50.0, -50.0),
               (-25.0, -75.0), (17.0, -75.0),
               (-25.0, -100.0), (17.0, -100.0)]
    bus_type = ['b', 'b', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm', 'm']
    nominal_voltage = [12.47, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24, 0.24]

    assert(len(geodata) == n_buses and len(bus_type) == n_buses)

    for i in range(n_buses):
        pp.create_bus(net, name='R'+str(i), index=i, vn_kv=nominal_voltage[i], geodata=geodata[i],
                      type=bus_type[i], zone='CIGRE LV', in_service=True)

    # lines
    n_lines = 13
    from_bus = [1, 2, 3, 1, 1, 1, 2, 2, 2, 3, 3, 4, 4]
    to_bus = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    conductor_id = ['OH2', 'UG3', 'UG3', 'OH3', 'OH3', 'OH3', 'OH3', 'OH3', 'OH3', 'UG4', 'UG4', 'UG4', 'UG4']
    line_length = [25.0, 25.0, 25.0, 25.0, 25.0, 50.0, 25.0, 25.0, 50.0, 17.0, 25.0, 17.0, 25.0]

    # line types and their specifications - REMARK: only found R values in CIGRE documentation,
    # rest is by comparing to closest standard line type
    oh2 = {'r_ohm_per_km': 0.209, 'x_ohm_per_km': 0.32, 'c_nf_per_km': 10.5, 'max_i_ka': 0.47}
    oh3 = {'r_ohm_per_km': 1.092, 'x_ohm_per_km': 0.3, 'c_nf_per_km': 11.25, 'max_i_ka': 0.14}
    ug3 = {'r_ohm_per_km': 0.343, 'x_ohm_per_km': 0.13, 'c_nf_per_km': 216.0, 'max_i_ka': 0.252}
    ug4 = {'r_ohm_per_km': 1.092, 'x_ohm_per_km': 0.08, 'c_nf_per_km': 210.0, 'max_i_ka': 0.12}
    pp.create_std_type(net, oh2, name='OH2', element='line')
    pp.create_std_type(net, oh3, name='OH3', element='line')
    pp.create_std_type(net, ug3, name='UG3', element='line')
    pp.create_std_type(net, ug4, name='UG4', element='line')

    for i in range(n_lines):
        pp.create_line(net, from_bus=from_bus[i], to_bus=to_bus[i], length_km=line_length[i]*1e-3,
                       std_type=conductor_id[i], name='Line R' + str(from_bus[i]) + '-R' + str(to_bus[i]), index=i)

    # external grid
    pp.create_ext_grid(net, bus=0, vm_pu=1.0, va_degree=0.0,
                       s_sc_max_mva=100.0, s_sc_min_mva=100.0, rx_max=1.0, rx_min=1.0)
    # transformer
    pp.create_transformer_from_parameters(net, hv_bus=0, lv_bus=1, sn_mva=0.05, vn_hv_kv=12.47, vn_lv_kv=0.24,
                                          vkr_percent=0.4, vk_percent=4.0, pfe_kw=1.5, i0_percent=0.25)
    # loads & generation
    buses_with_house = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    for i, bus in enumerate(buses_with_house):
        pp.create_load(net, bus=buses_with_house[i], index=i, p_mw=0, q_mvar=0, name='Load R'+str(bus))
        pp.create_sgen(net, bus=buses_with_house[i], index=i, p_mw=0, q_mvar=0, name='SGen R'+str(bus))

    return net
