from Paper.Code.data import Prepare_Data as data
from Paper.Code.grid import Prepare_CIGRE_Residential as cigre
import numpy as np
import datetime

# find the maximum PV generation from the data of a whole year, which is assumed to be
# equal to the power rating of the available PV inverters

start_date = datetime.date(year=2018, month=7, day=2)
end_date = datetime.date(year=2018, month=7, day=2)
n_days = (end_date-start_date).days+1

res = '1min'
location = 'austin'

# grid
load_bus_configuration = 'original'  # keeps the number of load buses at 5 as in the original CIGRE documentation
n_loads_per_bus = {'R11': 3, 'R15': 4, 'R16': 5, 'R17': 6, 'R18': 7}  # vary the number of loads/generation connected
load_scaling = 2.0
pv_scaling = 2.0

net = cigre.prepare_european_cigre_lv_residential(load_buses=load_bus_configuration)

max_load = np.zeros(5)
max_gen = np.zeros(5)

for i in range(n_days):

    date = start_date + datetime.timedelta(days=i)
    day = date.day
    month = date.month
    print(date)

    t, load_data, gen_data, date = data.prepare_1day_deterministic(net, location, res, day, month,
                                                                   n_loads_per_bus, load_scaling, pv_scaling)

    max_load_day = np.amax(load_data, axis=0)
    max_gen_day = np.amax(gen_data, axis=0)

    max_load = np.maximum(max_load, max_load_day)
    max_gen = np.maximum(max_gen, max_gen_day)

print('Max Load: ', max_load)
print('Max Generation: ', max_gen)