# This script computes the sensitivity matrix dv/dq needed for FO algorithms on the power flow manifold
# Adapted from Matlab Script by Lukas Ortmann

import numpy as np
import pandapower as pp


def numerical_simulation(net):

    n_loads = len(net.load.index)

    H = np.zeros((n_loads, n_loads))
    q_j = 0.0001
    q0 = 0.0
    v0 = 1.0  # equilibrium voltage at all buses (p.u.)

    for i in range(n_loads):    # rows of H
        for j in range(n_loads):    # cols of H
            # get bus index of current load i
            bus = net.load.bus[i]
            # apply q_inj at bus j
            gen_q = np.zeros(n_loads)
            gen_q[j] = q_j
            net.sgen.q_mvar = gen_q
            # run power flow and get voltage change at bus of load i
            pp.runpp(net)
            v_i = net.res_bus.vm_pu[bus]
            delta_v = v_i - v0
            delta_q = q_j - q0
            # calculate H entry from the obtained values
            H[i, j] = delta_v/delta_q

    # save as csv file
    np.savetxt("H_numerical.csv", H, delimiter=",")


def bracket(A):

    bracket_A = np.block([[A.real, -A.imag],
                          [A.imag, A.real]])
    return bracket_A


def R(u):
    v = np.abs(u)
    theta = np.angle(u, deg=False)
    R_u = np.block([[np.diag(np.cos(theta)), -np.diag(v) @ np.diag(np.sin(theta))],
                    [np.diag(np.sin(theta)), np.diag(v) @ np.diag(np.cos(theta))]])
    return R_u


def implicit_linearization_bolognani(net):

    pp.runpp(net)
    Y = net._ppc["internal"]["Ybus"].toarray()
    n = len(Y)

    N = np.block([[np.eye(n), np.zeros((n, n))],
                  [np.zeros((n, n)), np.eye(n)]])

    # count number of slack buses
    n_slack_buses = net.bus[net.bus.vn_kv > net.bus.vn_kv.iloc[-1]].count().loc['vn_kv']
    # include one slack bus (R0) but not the 2nd one if exists
    v = net.res_bus.vm_pu.to_numpy()[n_slack_buses-1:]
    theta = net.res_bus.va_degree.to_numpy()[n_slack_buses-1:]

    u = v * np.exp(1j*theta)

    A = np.block([(bracket(np.diag(np.conj(Y @ u))) + bracket(np.diag(u)) @ N @ bracket(Y)) @ R(u), -np.eye(2*n)])

    # Version 2 - from discussion with Lukas on 4/30/2021
    # F = F_power-flow-equations = 0 : R^2n
    # x = (v\v_sl, theta\theta_sl, p_sl, q_sl) : R^2n
    # y = (p\p_sl, q\q_sl, v_sl, theta_sl) : R^2n

    a = A[:, 1:n]
    b = A[:, 2*n]

    dF_dy = np.column_stack([A[:, 1:n], A[:, n+1:2*n], A[:, 2*n], A[:, 3*n]])
    dF_dx = np.column_stack([A[:, 2*n+1:3*n], A[:, 3*n+1:4*n], A[:, 0].T, A[:, n].T])

    # X = dy/dx = d(p\p_sl, q\q_sl, v_sl, theta_sl) / d(v\v_sl, theta\theta_sl, p_sl, q_sl)
    X = np.linalg.inv(dF_dy) @ dF_dx

    # H = dv/dq
    H = -X[n-1:2*n-2, 0:n-1]
    # only use buses which have a load/generation
    H = H[np.ix_(net.load.bus-n_slack_buses, net.load.bus-n_slack_buses)]

    # save as csv file
    np.savetxt("H.csv", H, delimiter=",")
