# --- GENERAL PARAMETERS USED FOR ALL SIMULATIONS --- #
import numpy as np

# data
res = '1min'
location = 'austin'

# FO control
alpha = 4.0
anti_windup = True
tighten_v_constraints = 0
max_fo_steps = 6

# scaling
pv_factors = {'2020': 1.0, '2030': 2.5, '2035': 3.5}    # different scenarios for extent of PV integration
load_scaling = 2.0  # scale load profiles to match CIGRE documentation in absolute terms
pv_scaling = {'2020': pv_factors['2020'] * load_scaling,
              '2030': pv_factors['2030'] * load_scaling,
              '2035': pv_factors['2035'] * load_scaling}
nominal_loads = {'R11': 15e-3,
                 'R15': 52e-3,
                 'R16': 55e-3,
                 'R17': 35e-3,
                 'R18': 47e-3,
                 }

# VAR limits of generators
# choose MVAr limits according to data -> availability of PV inverters, 30% of maximum generation at each bus
max_kW_gens = np.array([0.031118, 0.045562, 0.03911, 0.042646, 0.042828])   # MW
var_limits = {'2020': pv_factors['2020'] * 0.3 * max_kW_gens,
              '2030': pv_factors['2030'] * 0.3 * max_kW_gens,
              '2035': pv_factors['2035'] * 0.3 * max_kW_gens}

maximum_droop_slope = 1.8
low_pass_lambda = 0.8

# voltage limits (grid code)
v_min = 0.95    # p.u.
v_max = 1.05    # p.u.

# grid
load_bus_configuration = 'original'  # keeps the number of load buses at 5 as in the original CIGRE documentation
load_bus_idx = [12, 16, 17, 18, 19]
load_bus_names = ["R11", "R15", "R16", "R17", "R18", "R19"]
n_loads_per_bus = {'R11': 3, 'R15': 4, 'R16': 5, 'R17': 6, 'R18': 7}    # vary the number of loads/generation connected to each load bus to match CIGRE documentation

H_sensitivity_matrix = np.genfromtxt('grid/H.csv', delimiter=',')

# settings for metrics calculation
n_histo = 100   # number of buckets used for voltage histograms
histo_range = [0.92, 1.12]
