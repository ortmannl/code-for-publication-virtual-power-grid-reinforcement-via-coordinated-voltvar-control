import numpy as np
import Simulation_Settings as params


def voltage_histogram(v):

    # do not use voltages at external grid and transformer node (always 1 p.u.)
    v = v[2:, :]

    v_max = params.v_max
    v_min = params.v_min

    n_histo = params.n_histo
    histo_range = params.histo_range

    histo_step = (v_max - v_min) / int(n_histo * (v_max - v_min) / (histo_range[1] - histo_range[0]))
    steps_left = (v_min - histo_range[0]) // histo_step
    steps_right = (histo_range[1] - v_max) // histo_step

    # n_histo = int(n_histo + steps_left + steps_right + 1)
    histo_values = np.linspace(0.95 - histo_step / 2 - steps_left * histo_step,
                               1.05 + histo_step / 2 + steps_right * histo_step,
                               n_histo)

    voltage_histo = np.zeros(n_histo)
    for r in range(n_histo):
        voltage_histo[r] += np.count_nonzero(np.logical_and(histo_values[r] - histo_step / 2 < v,
                                                            v <= histo_values[r] + histo_step / 2))

    return {'x': histo_values, 'y': voltage_histo}


def calculate_time_series_metrics(v, q, p_loss, v_no_control=None):

    # do not use voltages at external grid and transformer node (always 1 p.u.)
    v = v[2:, :]
    if v_no_control is None:
        print("Voltages without control not availabel. Unnecessary line losses will be the same as total line losses.")
        v_no_control = np.ones(v.shape[0])

    timestep = int(params.res[:-3])
    if timestep not in [1, 15]:
        raise ValueError("Invalid Time Resolution")

    metrics = {
        'total injected Q (MVArh)': np.sum(q.clip(min=0)) * timestep / 60,
        'total consumed Q (MVArh)': -np.sum(q.clip(max=0)) * timestep / 60,
        'total injected Q squared (MVAr2h)': np.sum(q.clip(min=0)**2) * timestep / 60,
        'total consumed Q squared (MVAr2h)': np.sum(q.clip(max=0)**2) * timestep / 60,
        'maximum injected Q (MVAr)': np.amax(np.sum(q, axis=0)),
        'maximum consumed Q (MVAr)': -np.amin(np.sum(q, axis=0)),
        'maximum bus voltage (p.u.)': np.amax(v),
        'minimum bus voltage (p.u.)': np.amin(v),
        'duration of overvoltage (h)': timestep/60 * np.count_nonzero(v > params.v_max),
        'duration of undervoltage (h)': timestep/60 * np.count_nonzero(v < params.v_min),
        'integral of overvoltage (Vh)': timestep/60 * np.sum((v - params.v_max).clip(min=0)),
        'integral of undervoltage (Vh)': timestep/60 * np.sum((params.v_min - v).clip(min=0)),
        'total losses in grid (MWh)': np.sum(p_loss) * timestep / 60,
        'total unnecessary losses in grid (MWh)': np.sum(p_loss[(np.amax(v_no_control) < 1.04999) & (np.amin(v_no_control) > 0.95001)]) * timestep / 60
    }

    return metrics
